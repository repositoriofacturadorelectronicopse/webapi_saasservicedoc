﻿
/****************************************************************************************************************************************************************************************
 PROGRAMA: Configuracion.cs
 VERSION : 1.0
 OBJETIVO: Clase de funciones comunes
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

using System;
using System.Net;
using CEN;

namespace AppConfiguracion
{
    public class Configuracion
    {

    public static string CompletarCeros(string texto, short cantDigitos)
    {
         // DESCRIPCION: FUNCION PARA COMPLETAR CEROS
        int intCount;                       // CONTEO
        string cadena = string.Empty;       // CADENA
        int i;                              // ENTERO I
        intCount = texto.Length;
        int cantCeros;                      // CANTIDAD DE CEROS
        cantCeros = cantDigitos - intCount;
        if (cantCeros > 0)
        {
            var loopTo = cantCeros - 1;         // BUCLE
            for (i = 0; i <= loopTo; i++)
                cadena = cadena + "0";
            cadena = cadena + texto;
        }
        else
        {
            cadena = texto;
        }

        return cadena;
    }

       public string getDateNowLima()
        {

            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(EN_Constante.g_cost_nameTimeDefect);

            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone);
            return cstTime.ToString(EN_Constante.g_const_formfech);

        }
        public string getDateHourNowLima()
        {
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(EN_Constante.g_cost_nameTimeDefect);
            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone);
            return cstTime.ToString(EN_Constante.g_const_formhora);
        }



        public string GetCadenaConexionFromWA(string p_url,string p_puerto,string p_api, string p_region,  string p_version, string p_secrectname)
       {
        // DESCRIPCION: FUNCION PARA OBTENER CADENA DE CONEXION DESDE API SERVICE MANAGER


           string cadenaConexion= EN_Constante.g_const_vacio;
           string urlWA     = EN_Constante.g_const_vacio;
       

           try
           {
            
                urlWA= p_url+":"+p_puerto+"/"+p_api;


                using(WebClient webClient = new WebClient())
                {
                    webClient.QueryString.Add("secretName", p_secrectname);
                    webClient.QueryString.Add("region", p_region);
                    webClient.QueryString.Add("versionStage", p_version);
                    cadenaConexion = webClient.DownloadString(urlWA);
                    return cadenaConexion;

                }
                
            
           }
           catch (System.Exception ex)
           {
               cadenaConexion = EN_Constante.g_const_vacio;
               return cadenaConexion;
               
           }
       }

    
    
    }
}