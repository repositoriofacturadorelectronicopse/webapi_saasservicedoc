
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env
WORKDIR /app

# WORKDIR /home/jalarcon/Documentos/FacturacionElectronica/

# Copy csproj and restore as distinct layers
COPY SAASServiceDoc/*.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY . ./
RUN dotnet publish webapi_servicedoc.sln -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "SAASServiceDoc.dll"]


