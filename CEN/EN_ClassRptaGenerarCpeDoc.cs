/****************************************************************************************************************************************************************************************
 PROGRAMA: ClassRptaGenerarCpeDoc.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad  ClassRptaGenerarCpeDoc
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
namespace CEN
{
        public class ClassRptaGenerarCpeDoc
        {
            // DESCRIPCION: Registo respuesta comprobante documentos
           public EN_RptaRegistroCpeDoc  RptaRegistroCpeDoc { get; set; }
           public EN_ErrorWebService ErrorWebService { get; set; }
           public ClassRptaGenerarCpeDoc()
           {
            RptaRegistroCpeDoc  = new EN_RptaRegistroCpeDoc();
            ErrorWebService = new EN_ErrorWebService();
           }    

        }

        public class EN_RptaRegistroCpeDoc
        {
             // DESCRIPCION: Registo generar comprobante documentos
            public bool FlagVerificacion { get; set; }   //flag de verificación
            public string DescripcionResp { get; set; }   //Descripción de la respuesta
            public string MensajeResp { get; set; }   //Mensaje de la respuesta
            public int codigo { get; set; } // Código resultado
            public string estadoComprobante { get; set; } // estado del comprobante
            public EN_RptaRegistroCpeDoc()
            {
                FlagVerificacion= false;
                DescripcionResp= EN_Constante.g_const_vacio;
                MensajeResp = EN_Constante.g_const_vacio;
                codigo =  EN_Constante.g_const_0;
            }
        }
 
}