/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_Resumen.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad resumen
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System.Collections.Generic;
namespace CEN
{
    public class EN_Resumen
    {
       private int _Id;
    public int Id
    {
        get
        {
            return _Id;
        }
        set
        {
            _Id = value;
        }
    }

    private int _Empresa_id;
    public int Empresa_id
    {
        get
        {
            return _Empresa_id;
        }
        set
        {
            _Empresa_id = value;
        }
    }

    private string _Nrodocumento;
    public string Nrodocumento
    {
        get
        {
            return _Nrodocumento;
        }
        set
        {
            _Nrodocumento = value;
        }
    }

    private string _DescripcionEmpresa;
    public string DescripcionEmpresa
    {
        get
        {
            return _DescripcionEmpresa;
        }
        set
        {
            _DescripcionEmpresa = value;
        }
    }

    private string _Tiporesumen;
    public string Tiporesumen
    {
        get
        {
            return _Tiporesumen;
        }
        set
        {
            _Tiporesumen = value;
        }
    }

    private int _Correlativo;
    public int Correlativo
    {
        get
        {
            return _Correlativo;
        }
        set
        {
            _Correlativo = value;
        }
    }

    private string _FechaEmisionDocumento;
    public string FechaEmisionDocumento
    {
        get
        {
            return _FechaEmisionDocumento;
        }
        set
        {
            _FechaEmisionDocumento = value;
        }
    }

    private string _FechaGeneraResumen;
    public string FechaGeneraResumen
    {
        get
        {
            return _FechaGeneraResumen;
        }
        set
        {
            _FechaGeneraResumen = value;
        }
    }

    private string _IdentificaResumen;
    public string IdentificaResumen
    {
        get
        {
            return _IdentificaResumen;
        }
        set
        {
            _IdentificaResumen = value;
        }
    }

    private string _Estado;
    public string Estado
    {
        get
        {
            return _Estado;
        }
        set
        {
            _Estado = value;
        }
    }

    private string _Situacion;
    public string Situacion
    {
        get
        {
            return _Situacion;
        }
        set
        {
            _Situacion = value;
        }
    }

    private string _EnviadoMail;
    public string EnviadoMail
    {
        get
        {
            return _EnviadoMail;
        }
        set
        {
            _EnviadoMail = value;
        }
    }
    // Ultimos Parametros Agregados
    private string _nombreXML;
    public string nombreXML
    {
        get
        {
            return _nombreXML;
        }
        set
        {
            _nombreXML = value;
        }
    }
    private string _XMLEnviado;
    public string XMLEnviado
    {
        get
        {
            return _XMLEnviado;
        }
        set
        {
            _XMLEnviado = value;
        }
    }
    private string _CDRXML;
    public string CDRXML
    {
        get
        {
            return _CDRXML;
        }
        set
        {
            _CDRXML = value;
        }
    }
    private string _vResumen;
    public string vResumen
    {
        get
        {
            return _vResumen;
        }
        set
        {
            _vResumen = value;
        }
    }
    private string _vFirma;
    public string vFirma
    {
        get
        {
            return _vFirma;
        }
        set
        {
            _vFirma = value;
        }
    }
    private string _ticket;
    public string ticket
    {
        get
        {
            return _ticket;
        }
        set
        {
            _ticket = value;
        }
    }

    // Para Busquedas
    private string _FechaIni;
    public string FechaIni
    {
        get
        {
            return _FechaIni;
        }
        set
        {
            _FechaIni = value;
        }
    }

    private string _FechaFin;
    public string FechaFin
    {
        get
        {
            return _FechaFin;
        }
        set
        {
            _FechaFin = value;
        }
    }

    // Para Resumen Enviado
    private string _MensajeCDR;
    public string MensajeCDR
    {
        get
        {
            return _MensajeCDR;
        }
        set
        {
            _MensajeCDR = value;
        }
    }

    private string _FechaEnvio;
    public string FechaEnvio
    {
        get
        {
            return _FechaEnvio;
        }
        set
        {
            _FechaEnvio = value;
        }
    }

    // Para Errores Rechazos
    private string _MensajeError;
    public string MensajeError
    {
        get
        {
            return _MensajeError;
        }
        set
        {
            _MensajeError = value;
        }
    }

    private string _FechaError;
    public string FechaError
    {
        get
        {
            return _FechaError;
        }
        set
        {
            _FechaError = value;
        }
    }

    // Informacion Auditoria
    private string _UsuarioSession;
    public string UsuarioSession
    {
        get
        {
            return _UsuarioSession;
        }
        set
        {
            _UsuarioSession = value;
        }
    }

    private string _CadenaAleatoria;
    public string CadenaAleatoria
    {
        get
        {
            return _CadenaAleatoria;
        }
        set
        {
            _CadenaAleatoria = value;
        }
    }

    // Para reportes
    private string _Cantidad;
    public string Cantidad
    {
        get
        {
            return _Cantidad;
        }
        set
        {
            _Cantidad = value;
        }
    }

    // Para Validar Usuario Certificado Empresa
    private string _Usuario;
    public string Usuario
    {
        get
        {
            return _Usuario;
        }
        set
        {
            _Usuario = value;
        }
    }

    private string _Clave;
    public string Clave
    {
        get
        {
            return _Clave;
        }
        set
        {
            _Clave = value;
        }
    } 
    }

    public class ResumenSunat : EN_Resumen
{
    public ResumenSunat()
    {
        ListaDetalleResumen = new List<EN_DetalleResumen>();
    }
    public string ValorResumen { get; set; }
    public string ValorFirma { get; set; }
    public string NombreFichero { get; set; }
    public List<EN_DetalleResumen> ListaDetalleResumen { get; set; }
}
}