/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_Constante.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad  constantes
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
namespace CEN
{
    public class EN_Constante
    {
      public const int g_const_menos1 = -1;   // Constante -1
        public const int g_const_0 = 0; // Constante 0
        public const int g_const_1 = 1; // Constante 1
         public const int g_const_2 = 2; // Constante 2
         public const int g_const_3 = 3; // Constante 3
         public const int g_const_4 = 4; // Constante 4
         public const int g_const_5 = 5; // Constante 5 
         public const int g_const_6 = 6; // Constante 6
         public const int g_const_7 = 7; // Constante 7 
         public const int g_const_8 = 8; // Constante 8
         public const int g_const_9 = 9; // Constante 9 
         public const int g_const_10 = 10; // Constante 10
         public const int g_const_11 = 11; // Constante 11 
         public const int g_const_22 = 22; // Constante 22
         public const int g_const_23 = 23; // Constante 23
         public const int g_const_24 = 24; // Constante 24
         public const int g_const_25 = 25; // Constante 25
        public const string g_const_null = null; // Constante null
        public const string g_const_vacio = ""; // Constante vacio
        public const string g_const_formfech = "dd/MM/yyyy"; // formato de fecha

        public const string g_const_formfecha_yyyyMMdd = "yyyyMMdd"; // formato de fecha
        public const string g_const_formhora = "HH:mm:ss"; // formato de hora
        public const string g_const_rango_num = "[0-9]"; // Rango número
        
        public const int g_const_prefijo_cpe_doc = 2; // Prefijo de comprobante electronico doc
        public const int  g_const_100 = 100; // Constante 100
        public const int  g_const_101 = 101; // Constante 101
        public const int  g_const_102 = 102; // Constante 102
        public const int  g_const_103 = 103; // Constante 103
        public const int  g_const_104= 104; // Constante 104
        public const int  g_const_105 = 105; // Constante 105
        public const int  g_const_106 = 106; // Constante 106
        public const int  g_const_107 = 107; // Constante 107
        public const int  g_const_108 = 108; // Constante 108
        public const int  g_const_109 = 109; // Constante 109
        public const int  g_const_110 = 110; // Constante 110
        public const int  g_const_111 = 111; // Constante 111
        public const int  g_const_112 = 112; // Constante 112
        public const int  g_const_113 = 113; // Constante 113
        public const int  g_const_114 = 114; // Constante 114
        public const int  g_const_115 = 115; // Constante 115
        public const int  g_const_116 = 116; // Constante 116
        public const int  g_const_117 = 117; // Constante 117
        public const int  g_const_118 = 118; // Constante 118
        public const int  g_const_119 = 119; // Constante 119
        public const int  g_const_150 = 150; // Constante 150
        public const int  g_const_151 = 151; // Constante 151
        public const int  g_const_152 = 152; // Constante 152
        public const int  g_const_153 = 113; // Constante 113
        public const int  g_const_154 = 154; // Constante 154
        public const int  g_const_155 = 155; // Constante 155
        public const int  g_const_160 = 160; // Constante 160
        public const int  g_const_161 = 161; // Constante 161
        public const int  g_const_162 = 162; // Constante 162
        public const int  g_const_163 = 163; // Constante 163
        public const int  g_const_164 = 164; // Constante 164
        public const int  g_const_165 = 165; // Constante 165
        public const int  g_const_166 = 166; // Constante 166
        public const int  g_const_167 = 167; // Constante 167
        public const int  g_const_168 = 168; // Constante 168
        public const int  g_const_169 = 169; // Constante 169
        public const int  g_const_170 = 170; // Constante 170
        
         public const int g_const_1000 = 1000; // Constante 1000
        public const int g_const_1001 = 1001; // Constante 1001
        public const int g_const_1003 = 1003; // Constante 1003
        public const int g_const_1005 = 1005; // Constante 1005
        public const int g_const_1006 = 1006; // Constante 1006
        public const int g_const_1007 = 1007; // Constante 1007
        public const int g_const_2000 = 2000; // Constante 2000
        public const int g_const_2001 = 2001; // Constante 2001
        public const int g_const_2002 = 2002; // Constante 2002
        public const int g_const_2003 = 2003; // Constante 2003
        public const int g_const_3000 = 3000; // Constante 3000
        public const int g_const_4000 = 4000; // Constante 4000
        public const int g_const_4001 = 4001; // Constante 4001
        public const int g_const_1020 = 1020; // Constante 1020
        
        public const int g_const_1021 = 1021; // Constante 1021


        public const string g_const_cultureEsp="es-PE"; //constante culture español
        public const string g_cost_nameTimeDefect= "America/Bogota"; // Constante timeStandar
        public const int g_const_length_numero_cpe_doc = 8; // Conteo de caracteres campo numero de serie de documento electrónico
        public const int g_const_length_id_cpe_doc = 15; // Conteo de caracteres campo ID de documento electrónico

        public const string g_const_rutaSufijo_xml = "XML/"; // ruta sufijo para xml
        public const string g_const_rutaSufijo_pdf = "PDF/"; // ruta sufijo para pdf
        public const string g_const_rutaSufijo_cdr = "CDR/"; // ruta sufijo para cdr
        public const string g_const_extension_xml = ".XML"; //  extensión para xml
        public const string g_const_extension_zip = ".ZIP"; //  extensión para zip
        public const string g_const_extension_pdf = ".PDF"; //  extensión para pdf
        public const string g_const_prefijoXmlCdr_respuesta = "R-"; // prefijo para xml y cdr respuesta

        public const string g_const_si = "SI"; // SI
        public const char g_const_prefijo_Factura = 'F'; // prefijo de comprobante factura F001=F
        public const string g_const_situacion_pendiente= "P"; // Constante de situación pendiente
        public const string g_const_situacion_error= "E"; // Constante de situación error
        public const string g_const_situacion_observado= "O"; // Constante de situación observado
        public const string g_const_situacion_rechazado= "R"; // Constante de situación rechazado
        public const string g_const_situacion_aceptado= "A"; // Constante de situación aceptado
        public const string g_const_situacion_baja= "B"; // Constante de situación aceptado

         public const string g_const_estado_error= "E"; // Constante de estado error
         public const string g_const_estado_rechazada= "R"; // Constante de estado rechazada



        public const string g_const_valExito = "Operación Exitosa"; // Constante de éxito para registro log fin
        public const string g_const_errlogCons = "Error al ejecutar método "; // Constante de error para registro log fin
        
        
  
        //errores
        public const string g_const_error_interno = "Hubo un problema interno, por favor vuelva a intentar en unos momentos."; // Error interno general
        public const string g_const_error_envioxml = "Ocurrió un error al enviar documento electrónico"; // Error envio xml 
        
          public const string g_const_err_flagOse = "POR FAVOR VERIFIQUE FLAGOSE EN TRAMA - "+g_const_programa;//Constante de error de flagOse
          public const string g_const_err_certUserName = "POR FAVOR VERIFIQUE NOMBRE DE USUARIO DE CERTIFICADO EN TRAMA - "+g_const_programa;//Constante de error de certUserName
          public const string g_const_err_certClave = "POR FAVOR VERIFIQUE CLAVE DE CERTIFICADO EN TRAMA - "+g_const_programa;//Constante de error de certClave
          public const string g_const_err_rutaDoc = "POR FAVOR VERIFIQUE RUTA DE SERVIDOR  EN TRAMA - "+g_const_programa;//Constante de error de rutaDoc
          public const string g_const_err_nombreArchivo = "POR FAVOR VERIFIQUE NOMBRE DEL ARCHIVO XML  EN TRAMA - "+g_const_programa;//Constante de error de nombreArchivo
          public const string g_const_err_documentoId = "POR FAVOR VERIFIQUE ID DE DOCUMENTO EN TRAMA - "+g_const_programa;//Constante de error de documentoId
          public const string g_const_err_empresaId = "POR FAVOR VERIFIQUE ID DE EMPRESA EN TRAMA - "+g_const_programa;//Constante de error de empresaId
          public const string g_const_err_puntoventaId = "POR FAVOR VERIFIQUE ID DE PUNTO DE VENTA EN TRAMA - "+g_const_programa;//Constante de error de punto de venta id
          public const string g_const_err_tipodocumentoId = "POR FAVOR VERIFIQUE ID DE TIPO DE DOCUMENTO EN TRAMA - "+g_const_programa;//Constante de error de tipodocumentoId
          public const string g_const_err_Produccion = "POR FAVOR VERIFIQUE CAMPO PRODUCCION EN TRAMA - "+g_const_programa;//Constante de error de Produccion
          public const string g_const_err_fecha= "POR FAVOR VERIFIQUE FECHA EN TRAMA - "+g_const_programa;//Constante de error de fecha
          public const string g_const_err_Serie= "POR FAVOR VERIFIQUE SERIE EN TRAMA - "+g_const_programa;//Constante de error de Serie
          public const string g_const_err_Numero= "POR FAVOR VERIFIQUE NUMERO EN TRAMA - "+g_const_programa;//Constante de error de Numero

           public const string g_const_err_cpependientesenvio_cb = "DOCUMENTO(S) PENDIENTE(S) DE ENVIO.";//documentos pendientes de envio
          
          
        public const string g_const_programa = "SAAServiceDoc" ; // Nombre del Web Api
        public const string g_const_url = "http://localhost/SAAServiceDoc" ; // url de servicio
        public const string g_const_user_log= "IDE" ; // usuario de log
         public const string g_const_funcion_generarCpe= "enviarComprobanteDoc" ; // nombre método

        public const string g_const_sufijoRutaTemporalPSE = "/PSEtmp"; //sufijo de ruta temporal

        
    }
}