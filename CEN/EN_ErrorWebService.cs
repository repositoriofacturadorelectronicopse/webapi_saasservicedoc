/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_ErrorWebService.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad error web service
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
namespace CEN
{
    public class EN_ErrorWebService
    {
        public int TipoError { get; set; }
        public int CodigoError { get; set; }
        public string DescripcionErr {get;set;}

        public EN_ErrorWebService()
        {
            TipoError= EN_Constante.g_const_0;
            CodigoError= EN_Constante.g_const_0;
            DescripcionErr= EN_Constante.g_const_vacio;
        }
    }
}