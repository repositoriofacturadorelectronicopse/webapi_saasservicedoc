/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_RequestEnvioComBaja.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad request comunicado baja
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
namespace CEN
{
    public class EN_RequestEnvioComBaja
    {
     public string flagOse { get; set;}
     public string Produccion { get; set;}
     public string certUserName { get; set;}
     public string certClave { get; set;}
     public string rutaDoc { get; set;}
     public string ruc { get; set;}
     public string nombreArchivo { get; set;}
     public string resumenId { get; set;}
     public string empresaId { get; set;}
    public string Idpuntoventa { get; set;}
     public string tiporesumenId { get; set;}
    
     public string FechaComunicacion { get; set;}
    public string correlativo { get; set;}


  
     


    }
}