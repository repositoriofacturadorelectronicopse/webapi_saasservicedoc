/****************************************************************************************************************************************************************************************
 PROGRAMA: DAO_ConfigConstantes.cs
 VERSION : 1.0
 OBJETIVO: Clase de configuración de constantes
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System.IO;
using Microsoft.Extensions.Configuration;
using CLN;
using CEN;
using System;
using AppConfiguracion;

namespace AppServicioDoc
{
    public class DAO_ConfigConstantes
    {

        private readonly IConfiguration _configuration;
     
        public DAO_ConfigConstantes()
        {
            // DESCRIPCION: CONSTRUCTOR DE CLASE CONFIGURACION CONSTANTES
              
               var builder = new ConfigurationBuilder() //CONSTRUCTOR
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json",optional:true, reloadOnChange:true);
                _configuration = builder.Build();
        }

         public void SetCadenaConexion()
        {
              // DESCRIPCION: GUARDAR CONSTANTES DE CONFIGURACION CADENA DE CONEXION

            Configuracion config = new Configuracion();

            string const_ServiceManager_UrlWebApi;
            string const_ServiceManager_Puerto;
            string const_ServiceManager_ApiService;
            string const_ServiceManager_Region;
            string const_ServiceManager_VersionStage;
            string const_ServiceManager_SecretName;

            try
            {
                const_ServiceManager_UrlWebApi             = _configuration["AppSeettings:ServiceManager_UrlWebApi"];
                const_ServiceManager_Puerto                = _configuration["AppSeettings:ServiceManager_Puerto"];
                const_ServiceManager_ApiService            = _configuration["AppSeettings:ServiceManager_ApiService"];
                const_ServiceManager_Region                = _configuration["AppSeettings:ServiceManager_Region"];
                const_ServiceManager_VersionStage          = _configuration["AppSeettings:ServiceManager_VersionStage"];

                const_ServiceManager_SecretName            = (_configuration["AppSeettings:BaseDatosAmbiente"]=="desarrollo")? _configuration["AppSeettings:ServiceManager_SecretNameDesarrollo"]:_configuration["AppSeettings:ServiceManager_SecretNameProduccion"];

                EN_ConfigConstantes.Instance.const_cadenaCnxBdFE= config.GetCadenaConexionFromWA(const_ServiceManager_UrlWebApi, const_ServiceManager_Puerto, const_ServiceManager_ApiService, const_ServiceManager_Region, const_ServiceManager_VersionStage, const_ServiceManager_SecretName);

            }
            catch (System.Exception ex)
            {
                
                throw ex;
            }

       
        }


        public void guardar_ConfigConstantes()
        {
            // DESCRIPCION: GUARDAR CONSTANTES DE CONFIGURACION
            
            NE_Facturacion ne_documento = new NE_Facturacion();     // CLASE DE NEGOCIO DE FACTURACION
            
            // EN_ConfigConstantes.Instance.const_cadenaCnxBdFE= _configuration.GetConnectionString("ConexionBdFE");

            if(EN_ConfigConstantes.Instance.const_cadenaCnxBdFE==  EN_Constante.g_const_vacio || EN_ConfigConstantes.Instance.const_cadenaCnxBdFE== null)
            {
                SetCadenaConexion();
            }
            
            EN_ConfigConstantes.Instance.const_TiempoEspera = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_110);  // TIEMPO DE ESPERA EJECUTAR QUERY- APPSETTINGS

            EN_ConfigConstantes.Instance.const_codigoEtiquetaErrorDoc = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_116);  //  CODIGO DE ETIQUETA DE ERROR DOC - APPSETTINGS
            EN_ConfigConstantes.Instance.const_codigoEtiquetaErrorRes = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_117);  // CODIGO DE ETIQUETA DE ERROR RESUMEN - APPSETTINGS
            
            // constantes para endpoints
            EN_ConfigConstantes.Instance.const_BetaSunatDoc         = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_160);  // ENDPOINT BETA SUNAT DOCUMENTO ELECTRONICO 
            EN_ConfigConstantes.Instance.const_BetaSunatOtrosCpe    = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_161);  // ENDPOINT BETA SUNAT OTROS DOCUMENTOS ELECTRONICOS
            EN_ConfigConstantes.Instance.const_BetaSunatGuias       = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_162);  // ENDPOINT BETA SUNAT GUIAS
            EN_ConfigConstantes.Instance.const_ProdSunatDoc         = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_163);  // ENDPOINT PRODUCCION SUNAT DOCUMENTO ELECTRONICO
            EN_ConfigConstantes.Instance.const_ProdSunatOtrosCpe    = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_164);  // ENDPOINT PRODUCCION SUNAT OTROS DOCUMENTOS ELECTRONICOS
            EN_ConfigConstantes.Instance.const_ProdSunatGuias       = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_165);  // ENDPOINT PRODUCCION SUNAT GUIAS
            EN_ConfigConstantes.Instance.const_consultaSunatService = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_166);  // ENDPOINT CONSULTA SUNAT SERVICE
            EN_ConfigConstantes.Instance.const_BetaNubefact         = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_167);  // ENDPOINT BETA NUBEFACT
            EN_ConfigConstantes.Instance.const_ProdNubefact         = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_168);  // ENDPOINT PRODUCCION NUBEFACT
            EN_ConfigConstantes.Instance.const_BetaEfac             = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_169);  // ENDPOINT BETA EFACT
            EN_ConfigConstantes.Instance.const_ProdEfac             = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_170);  // ENDPOINT PRODUCCCION EFACT
            
            guardar_ConfigConstantes_S3();

    
        }

        public void guardar_ConfigConstantes_S3()
        {
            // DESCRIPCION: GUARDAR CONSTANTES DE CONFIGURACION DE S3 AWS

            EN_Parametro res_par;              // clase entidad parametro para resultado
            EN_Parametro bus_par;              // clase entidad parametro para búsqueda
            NE_Proceso ne_proceso = new NE_Proceso();     // CLASE DE NEGOCIO DE EMPRESA

             try
            {

                res_par = new EN_Parametro();  
                bus_par = new EN_Parametro();
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_22;
                res_par =ne_proceso.buscar_tablaParametro(bus_par);
                EN_ConfigConstantes.Instance.const_s3_accessKey = res_par.par_descripcion;

                res_par = new EN_Parametro();  
                bus_par = new EN_Parametro();
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_23;
                res_par =ne_proceso.buscar_tablaParametro(bus_par);
                EN_ConfigConstantes.Instance.const_s3_secretKey = res_par.par_descripcion;

                res_par = new EN_Parametro();  
                bus_par = new EN_Parametro();
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_24;
                res_par =ne_proceso.buscar_tablaParametro(bus_par);
                EN_ConfigConstantes.Instance.const_s3_region = res_par.par_descripcion;

                res_par = new EN_Parametro();  
                bus_par = new EN_Parametro();
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_25;
                res_par =ne_proceso.buscar_tablaParametro(bus_par);
                EN_ConfigConstantes.Instance.const_s3_bucketName = res_par.par_descripcion;

                string ruta=Directory.GetCurrentDirectory()+EN_Constante.g_const_sufijoRutaTemporalPSE;
                
                if ((!System.IO.Directory.Exists(ruta)))
                {
                    System.IO.Directory.CreateDirectory(ruta);
                }

               

                EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos = ruta;
            }
            catch(Exception ex)
            {
                throw ex;

            }

    
        }
     

 
    }
}