/****************************************************************************************************************************************************************************************
 PROGRAMA: PasswordDigestMessageInspector.cs
 VERSION : 1.0
 OBJETIVO: Clase password inspector para envio en soap
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Xml;
using System.Xml.Serialization;

namespace AppServicioDoc
{
   
   public partial class PasswordDigestMessageInspector : IClientMessageInspector
{
        public string username {get; set; } // NOMBRE DE USUARIO
        public string password {get; set; } // PASSWORD
   

    public PasswordDigestMessageInspector(string username__1, string password__2)
    {
        // DESCRIPCION: CONSTRUCTOR DE CLASE
        username = username__1;
        password = password__2;
    }

    public void AfterReceiveReply(ref Message reply, object correlationState)
    {
        // DESCRIPCION: DESPUES DE RECIBIR RESPUESTA
        return;
    }

    public object BeforeSendRequest(ref Message request, IClientChannel channel)
    {
        // DESCRIPCION: ANTES DE ENVIAR SOLICITUD

       var header = new Security        // CABECERA
		{
			UsernameToken =
			{
				Password = new Password
				{
					Value = password,
					Type =
						"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText"
				},
				Username = username
			}
		};
		request.Headers.Add(header);
		return null;
    }

    public class Password
    {
            [XmlAttribute] public string Type { get; set; }

            [XmlText] public string Value { get; set; }
    }

        [XmlRoot(Namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")]
    public class UsernameToken
    {
        [XmlElement] public string Username { get; set; }

        [XmlElement] public Password Password { get; set; }
    }

    public class Security : MessageHeader
    {
        public Security()
        {
            UsernameToken = new UsernameToken();
        }

        public UsernameToken UsernameToken { get; set; }

        public override string Name => GetType().Name;

        public override string Namespace =>
            "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";

        public override bool MustUnderstand => true;

        protected override void OnWriteHeaderContents(XmlDictionaryWriter writer, MessageVersion messageVersion)
        {
            var serializer = new XmlSerializer(typeof(UsernameToken));
            serializer.Serialize(writer, UsernameToken);
        }
    }


}
}