/****************************************************************************************************************************************************************************************
 PROGRAMA: GenerarXml.cs
 VERSION : 1.0
 OBJETIVO: Clase generar xml
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using CEN;
using System.IO;
using Microsoft.VisualBasic;
using System.Xml;
using System.Text;
using CLN;
using AppConfiguracion;
using System.IO.Compression;
using System.Collections.Generic;
using System.Timers;

namespace AppServicioDoc
{
    public class GenerarXML
    {
        
 
    
    public static string ValidarSituacion(EN_Documento pDocumento)
    {
        // DESCRIPCION: VALIDA SITUACION DE DOCUMENTO

        var objDoc = new NE_Documento();                //  CLASE NEGOCIO DOCUMENTO
        return objDoc.consultarSituacion(pDocumento);
    }

    public EN_Resumenfirma GetResumenFirma(EN_Documento oDocumento)
    {
        // DESCRIPCION: DEVUELVE RESUMEN FIRMA

        var objDoc = new NE_Documento();                //  CLASE NEGOCIO DOCUMENTO
        return objDoc.GetResumenFirma(oDocumento);

    }
    public EN_Resumenfirma GetResumenFirmaRD(EN_Resumen oDocumento)
    {
        // DESCRIPCION: DEVUELVE RESUMEN FIRMA DE RESUMEN DIARIO
        var objDoc = new NE_Documento();                 //  CLASE NEGOCIO DOCUMENTO
        return objDoc.GetResumenFirmaRD(oDocumento);

    }

    public EN_Resumenfirma GetResumenFirmaCB(BajaSunat oDocumento)
    {
        // DESCRIPCION: DEVUELVE RESUMEN FIRMA DE RESUMEN DIARIO
        var objDoc = new NE_Documento();                 //  CLASE NEGOCIO DOCUMENTO
        return objDoc.GetResumenFirmaCB(oDocumento);

    }

    public  bool EnviarXmlDocumentoElectronico(EN_Documento oDocumento, EN_Certificado oCertificado, EN_ComprobanteSunat oCompSunat, string produccion, string nombre, string vResumen, string vFirma,  ref string estadoCDR, ref string msjCDR)
    {
        // DESCRIPCION: Este metodo se encarga de enviar a SUNAT el comprobante generado
        
        string ruta= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos+"/"+oDocumento.NroDocEmpresa+"/";
        string cdrXML = EN_Constante.g_const_vacio;                 // XML DE CDR            
        string rutaXml=ruta + EN_Constante.g_const_rutaSufijo_xml;         // RUTA XML
        string rutaCdr=ruta + EN_Constante.g_const_rutaSufijo_cdr;         // RUTA CDR

        try
        {
        
             //COMPROBAMOS CARPETAS Y ARCHIVOS INICIALES  DE S3 Y LOCAL TEMPORAL
            AppConfiguracion.FuncionesS3.InitialS3(oDocumento.NroDocEmpresa);

             // CREAMOS TODOS LOS DIRECTORIOS NECESARIOS. SI NO EXISTE
            if ((!System.IO.Directory.Exists(ruta.Trim() + EN_Constante.g_const_rutaSufijo_xml)))
                System.IO.Directory.CreateDirectory(ruta.Trim() + EN_Constante.g_const_rutaSufijo_xml);

            if ((!System.IO.Directory.Exists(ruta.Trim() + EN_Constante.g_const_rutaSufijo_cdr)))
                System.IO.Directory.CreateDirectory(ruta.Trim() + EN_Constante.g_const_rutaSufijo_cdr);
   

            // COPIAMOS EL XML DE S3 A LOCAL
            string s3RutaFile   = string.Format("/{0}/{1}",oDocumento.NroDocEmpresa, EN_Constante.g_const_rutaSufijo_xml+ nombre.Trim() + EN_Constante.g_const_extension_xml);
            string localRutaFile= rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml;
            AppConfiguracion.FuncionesS3.DownloadFile(s3RutaFile,localRutaFile).Wait();


            string ticket = EN_Constante.g_const_vacio;
            // Validamos el estado del comprobante en BD
            string sitdoc;
            sitdoc = ValidarSituacion(oDocumento);
            if (sitdoc == EN_Constante.g_const_situacion_pendiente || sitdoc == EN_Constante.g_const_situacion_error || string.IsNullOrEmpty(sitdoc))
            {
                // Validamos si existe comprobante en ZIP y PDF generado y eliminamos
                if (File.Exists(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_zip))
                {
                    File.Delete(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_zip);
                }
                // Validamos si existe CDR en XML y ZIP y eliminamos
                if (File.Exists(rutaCdr.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_xml))
                {
                    File.Delete(rutaCdr.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_xml);
                }

                if (File.Exists(rutaCdr.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip))
                {
                    File.Delete(rutaCdr.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip);
                }

                // Guardamos el XML en ZIP
                using (var zip = new Ionic.Zip.ZipFile(Encoding.UTF8))
                {
                    zip.AddFile(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml, EN_Constante.g_const_vacio);
                    zip.Save(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_zip);
                }
            
                // ZipFile.CreateFromDirectory(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml,rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_zip);


                // Enviamos el zip a SUNAT
                var xmlenviado = EnviarXMLAsync(oCertificado, produccion,ruta, nombre, oCompSunat.codigoEtiquetaErrorDoc, oDocumento.Fecha, ref cdrXML, ref estadoCDR, ref msjCDR, ref ticket);


                if (estadoCDR.Trim() == EN_Constante.g_const_situacion_error || estadoCDR.Trim() == EN_Constante.g_const_situacion_rechazado)
                {
                    // Actualizamos el estado del documento y guardamos el mensaje de error en BD
                    GuardarDocumentoRechazadoError(oDocumento, nombre, xmlenviado, cdrXML, vResumen, vFirma, msjCDR, estadoCDR.Trim(), ticket);
                    return false;

                }
                else
                {
                    if (!string.IsNullOrEmpty(cdrXML.Trim()))
                    {
                        // Actualizamos el estado del documento a Aceptado y guardamos el cdrXML BD
                        if(estadoCDR==EN_Constante.g_const_situacion_aceptado)
                            GuardarDocumentoAceptado(oDocumento, nombre, xmlenviado, cdrXML, vResumen, vFirma, msjCDR, ticket );
                        else
                            GuardarDocumentoObservado(oDocumento, nombre, xmlenviado, cdrXML, vResumen, vFirma, msjCDR, ticket,estadoCDR.Trim() );
                    }

                    return true;
                }
            }
            else
            {
                msjCDR = "El comprobante fue enviado con anterioridad, revisar.";
                return false;
            }
        }
        catch (Exception ex)
        {
            msjCDR = "Error: " + ex.Message;
            GuardarDocumentoRechazadoError(oDocumento, nombre, "No pudo generarse.", "No pudo generarse.", "-", "-", "Error: " + ex.Message, EN_Constante.g_const_situacion_error, EN_Constante.g_const_vacio);
            return false;
        }
        finally
        {

            //COPIAMOS XML A S3 Y ELIMINAMOS DE TEMPORAL
            AppConfiguracion.FuncionesS3.CopyS3DeleteLocal(oDocumento.NroDocEmpresa,  EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos,  EN_Constante.g_const_rutaSufijo_cdr, EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim(),  EN_Constante.g_const_extension_xml);
            
            //Eliminamos el xml copiado del s3
            if (File.Exists(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml))
            {
                File.Delete(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml);
            }

            // Eliminamos los ZIP
            if (File.Exists(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_zip))
            {
                File.Delete(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_zip);
            }

            if (File.Exists(rutaCdr.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip))
            {
                File.Delete(rutaCdr.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip);
            }
        }
    }

    private string EnviarXMLAsync(EN_Certificado oCertificado,string produccion, string rutaFE, string Nombre, string codEtiqueta, string fechadoc, ref string cdrXML, ref string estadoError, ref string msjError, ref string ticket)
    {
        // DESCRIPCION ENVIAR XML A SUNAT DE FORM ASINCRONA

        Configuracion config = new Configuracion();         // CONFIGURACION
        NE_Facturacion TablaFEADObj = new NE_Facturacion(); // CLASE NEGOCIO FACTURACION

        string rutaCdr=rutaFE+ EN_Constante.g_const_rutaSufijo_cdr;                       // RUTA CDR
        string rutaXml=rutaFE+ EN_Constante.g_const_rutaSufijo_xml;                       // RUTA XML

        string cadenaXMLEnv = EN_Constante.g_const_vacio;                           // CADENA DE XML ENVIADO
        XmlDocument xmlEnv = new XmlDocument();             // XML DOCUMENTO ENVIADO
        xmlEnv.PreserveWhitespace = false;

        using (FileStream fs = new FileStream(rutaXml + Nombre + EN_Constante.g_const_extension_xml, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        {
            xmlEnv.Load(fs);
        }

        cadenaXMLEnv = xmlEnv.OuterXml.ToString().Trim();

        string codigoerror = EN_Constante.g_const_vacio;                            // CODIGO DE ERROR
        string descripcionerror = EN_Constante.g_const_vacio;                       // DESCRIPCION DE ERROR
        FileStream fsRpta = new FileStream(rutaCdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + Nombre + EN_Constante.g_const_extension_zip, FileMode.Create);

        try
        {
            System.Net.ServicePointManager.UseNagleAlgorithm = true;
            System.Net.ServicePointManager.Expect100Continue = false;
            System.Net.ServicePointManager.CheckCertificateRevocationList = true;

            XmlDocument xmlCdr = new XmlDocument();         // XML DE CDR

                dynamic ws =null;                           // WEBSERVICE
                dynamic envio = null;                       // ENVIO
                string rutaSunat;                           // RUTA SUNAT      
                dynamic wsEnd;                              // WEBSERVICE ENDPOINT

                if(oCertificado.flagOSE == EN_Constante.g_const_1.ToString()){
                
                    if(produccion==EN_Constante.g_const_si) rutaSunat=EN_ConfigConstantes.Instance.const_ProdNubefact;
                    else rutaSunat= EN_ConfigConstantes.Instance.const_BetaNubefact;

                    wsEnd= new ServicioNubeFact.billServiceClient.EndpointConfiguration();
                    ws = new ServicioNubeFact.billServiceClient(wsEnd, rutaSunat);
                    envio = new ServicioNubeFact.sendBillRequest();
                
                }
                else if (oCertificado.flagOSE == EN_Constante.g_const_2.ToString())
                {   
                    if(produccion==EN_Constante.g_const_si) rutaSunat=EN_ConfigConstantes.Instance.const_ProdEfac;
                    else rutaSunat= EN_ConfigConstantes.Instance.const_BetaEfac;

                    wsEnd= new ServicioEfact.BillServiceClient.EndpointConfiguration();
                    ws = new ServicioEfact.BillServiceClient(wsEnd, rutaSunat);
                    envio = new ServicioEfact.sendBillRequest();
                }
                else
                {
                    if(produccion==EN_Constante.g_const_si) rutaSunat=EN_ConfigConstantes.Instance.const_ProdSunatDoc;
                    else rutaSunat= EN_ConfigConstantes.Instance.const_BetaSunatDoc;
                
                    wsEnd= new ServicioSunat.billServiceClient.EndpointConfiguration();
                    ws = new ServicioSunat.billServiceClient(wsEnd, rutaSunat);
                    envio= new ServicioSunat.sendBillRequest();
                }
            
                    ws.ClientCredentials.UserName.UserName = oCertificado.UserName;
                    ws.ClientCredentials.UserName.Password = oCertificado.Clave;

                    var behavior = new PasswordDigestBehavior(oCertificado.UserName, oCertificado.Clave);
                    ws.Endpoint.EndpointBehaviors.Add(behavior);

                    envio.fileName = Nombre + EN_Constante.g_const_extension_zip;
                    envio.contentFile = File.ReadAllBytes(rutaXml + Nombre + EN_Constante.g_const_extension_zip);

                    dynamic datosR=null;
                    if (oCertificado.flagOSE == EN_Constante.g_const_2.ToString())
                    { 
                        datosR=  ws.sendBillAsync(envio.fileName, envio.contentFile);
                    }else{
                    
                        datosR=  ws.sendBillAsync(envio.fileName, envio.contentFile,EN_Constante.g_const_vacio);
                    }
                
                    var datos= datosR.Result;
                    using(fsRpta)
                    {

                            fsRpta.Write(datos.applicationResponse, 0, datos.applicationResponse.Length);
                    }
                ws.Close();
                

                Extraer(rutaCdr, Nombre);
                xmlCdr.PreserveWhitespace = false;
                xmlCdr.Load(rutaCdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + Nombre + EN_Constante.g_const_extension_xml);
                cdrXML = xmlCdr.OuterXml.ToString().Trim();
                VerificarSunatCDR(xmlCdr.DocumentElement, codEtiqueta, ref msjError, ref codigoerror);
                VerificarSunatTicket(xmlCdr.DocumentElement, ref ticket);
        
            if (codigoerror != EN_Constante.g_const_vacio)
            {
                estadoError = TablaFEADObj.ObtenerCodigoAlternoPorCodigoSistema("999", codigoerror);
                descripcionerror = TablaFEADObj.ObtenerDescripcionPorCodElemento("999", codigoerror);
                if (estadoError == EN_Constante.g_const_situacion_rechazado)
                    msjError = "Comprobante rechazado: " + codigoerror + "-" + descripcionerror;
                else if (estadoError == EN_Constante.g_const_situacion_error)
                    msjError = "Comprobante con excepcion: " + codigoerror + "-" + descripcionerror;
                else if (estadoError == EN_Constante.g_const_situacion_observado)
                    msjError = "Comprobante observado: " + codigoerror + "-" + descripcionerror;
                else if (System.Convert.ToInt32(codigoerror) >= 100 & System.Convert.ToInt32(codigoerror) <= 1999)
                {
                    estadoError = EN_Constante.g_const_situacion_error;
                    msjError = "Comprobante con excepcion " + "-" + codigoerror;
                }
                else if (System.Convert.ToInt32(codigoerror) >= 2000 & System.Convert.ToInt32(codigoerror) <= 3999)
                {
                    estadoError = EN_Constante.g_const_situacion_rechazado;
                    msjError = "Comprobante rechazado " + "-" + codigoerror;
                }
                else if (System.Convert.ToInt32(codigoerror) >= 4000)
                {
                    estadoError = EN_Constante.g_const_situacion_observado;
                    msjError = "Comprobante obervado " + "-" + codigoerror;
                }
                else
                {
                    estadoError = EN_Constante.g_const_situacion_error;
                    msjError = "Error al enviar comprobante a la SUNAT " + "-" + codigoerror;
                }
            }
            else
            {
                //verificamos si el comprobante aceptado tiene observaciones
                   List<string> rsObs = new List<string>();
                xmlCdr.Load(rutaCdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + Nombre + EN_Constante.g_const_extension_xml);
                XmlNodeList xmlObs = xmlCdr.GetElementsByTagName("cbc:Note");

                foreach (XmlElement item in xmlObs)
                {
                    rsObs.Add(item.InnerText);

                }
                if (rsObs.Count == 0)
                {
                      estadoError = EN_Constante.g_const_situacion_aceptado;   
                }
                   
                else
                {
                    estadoError = EN_Constante.g_const_situacion_observado;   
                    msjError= msjError+ " con "+rsObs.Count +" Observacion(es)"       ;  
                }
                       
            }
                
        }
        catch (System.ServiceModel.FaultException ex)
        {
            codigoerror = ex.Code.Name.ToString().Replace(codEtiqueta, EN_Constante.g_const_vacio).Trim();
            estadoError = TablaFEADObj.ObtenerCodigoAlternoPorCodigoSistema("999", codigoerror);
            descripcionerror = TablaFEADObj.ObtenerDescripcionPorCodElemento("999", codigoerror);
            if (estadoError.Trim() == EN_Constante.g_const_vacio & Information.IsNumeric(ex.Message))
            {
                codigoerror = Configuracion.CompletarCeros((ex.Message), 4);
                estadoError = TablaFEADObj.ObtenerCodigoAlternoPorCodigoSistema("999", codigoerror);
                descripcionerror = TablaFEADObj.ObtenerDescripcionPorCodElemento("999", codigoerror);
            }
            if (estadoError == EN_Constante.g_const_situacion_rechazado)
                msjError = "Comprobante rechazado: " + codigoerror + "-" + descripcionerror;
            else if (estadoError == EN_Constante.g_const_situacion_error)
                msjError = "Comprobante con excepcion: " + codigoerror + "-" + descripcionerror;
            else if (estadoError == EN_Constante.g_const_situacion_observado)
                msjError = "Comprobante observado: " + codigoerror + "-" + descripcionerror;
            else if (System.Convert.ToInt32(codigoerror) >= 100 & System.Convert.ToInt32(codigoerror) <= 1999)
            {
                estadoError = EN_Constante.g_const_situacion_error;
                msjError = "Comprobante con excepcion " + "-" + codigoerror;
            }
            else if (System.Convert.ToInt32(codigoerror) >= 2000 & System.Convert.ToInt32(codigoerror) <= 3999)
            {
                estadoError = EN_Constante.g_const_situacion_rechazado;
                msjError = "Comprobante rechazado " + "-" + codigoerror;
            }
            else if (System.Convert.ToInt32(codigoerror) >= 4000)
            {
                estadoError = EN_Constante.g_const_situacion_observado;
                msjError = "Comprobante obervado " + "-" + codigoerror;
            }
            else
            {
                estadoError = EN_Constante.g_const_situacion_error;
                msjError = "Error al enviar comprobante a la SUNAT: " + ex.Code.Name.ToString() + "-" + ex.Message.Trim() + "-" + codigoerror;
            }
        }
        catch (Exception ex)
        {
            estadoError = EN_Constante.g_const_situacion_error;
            msjError = "Error al enviar comprobante a la SUNAT: " + ex.Message.Trim();
        }
        finally
        {
            if (fsRpta != null)
                fsRpta.Close();
        }
        return cadenaXMLEnv;
    }

    private static void VerificarSunatCDR(XmlElement nodos, string codEtiqueta, ref string msjnodo, ref string codigo)
    {
        // DESCRIPCION: VERIFICAR SUNAT CDR

        if (nodos.HasChildNodes)
        {
            if (nodos.ChildNodes[EN_Constante.g_const_0].NodeType != XmlNodeType.Text & nodos.ChildNodes[EN_Constante.g_const_0].NodeType != XmlNodeType.CDATA)
            {
                foreach (XmlElement nodo in nodos.ChildNodes)
                {
                    if (nodo.Name == "cac:DocumentResponse")
                    {
                        VerificarSunatCDR(nodo, codEtiqueta, ref msjnodo, ref codigo);
                    }
                    else if (nodo.Name == "cac:Response")
                    {
                        VerificarSunatCDR(nodo, codEtiqueta, ref msjnodo, ref codigo);
                    }
                    else if (nodo.Name == "cbc:ResponseCode")
                    {
                        if (nodo.FirstChild.Value.ToString().Replace(codEtiqueta, EN_Constante.g_const_vacio).Trim() != "0")
                        {
                            // Rechazada con motivo
                            codigo = nodo.FirstChild.Value.ToString().Replace(codEtiqueta, EN_Constante.g_const_vacio).Trim();
                            msjnodo = nodo.NextSibling.FirstChild.Value;
                        }
                        else
                        {
                            // Aceptada correctamente
                            codigo = EN_Constante.g_const_vacio;
                            msjnodo = nodo.NextSibling.FirstChild.Value;
                        }

                        break;
                    }
                }
            }
        }
    }

    private static void VerificarSunatTicket(XmlElement nodos, ref string ticket)
    {
        // DESCRIPCION: VERIFICAR SUNAT TICKET

        if (nodos.HasChildNodes)
        {
            if (nodos.ChildNodes[EN_Constante.g_const_0].NodeType != XmlNodeType.Text & nodos.ChildNodes[EN_Constante.g_const_0].NodeType != XmlNodeType.CDATA)
            {
                foreach (XmlElement nodo in nodos.ChildNodes)
                {
                    if (nodo.Name == "cbc:ID")
                    {
                        ticket = nodo.FirstChild.Value;
                        break;
                    }
                }
            }
        }
    }


    public static void GuardarDocumentoRechazadoError(EN_Documento oDocumento, string nombrexml, string xmlenviado, string cdrxml, string valorResumen, string firma, string errortext, string estado, string ticket)
    {
        // DESCRIPCION: GUARDAR DOCUMENTO RECHAZADO ERROR

        var objFacturacion = new NE_Facturacion();      // CLASE NEGOCIO FACTURACION
        try
        {
            objFacturacion.GuardarDocumentoRechazadoError(oDocumento, nombrexml, xmlenviado, cdrxml, valorResumen, firma, errortext, estado, ticket);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void GuardarDocumentoAceptado(EN_Documento oDocumento, string nombrexml, string xmlenviado, string cdrxml, string valorResumen, string firma, string mensaje, string ticket)
    {
          // DESCRIPCION: GUARDAR DOCUMENTO ACEPTADO

        var objFacturacion = new NE_Facturacion();      // CLASE NEGOCIO FACTURACION
        try
        {
            objFacturacion.GuardarDocumentoAceptado(oDocumento, nombrexml, xmlenviado, cdrxml, valorResumen, firma, mensaje, ticket);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
     public static void GuardarDocumentoObservado(EN_Documento oDocumento, string nombrexml, string xmlenviado, string cdrxml, string valorResumen, string firma, string mensaje, string ticket, string situacion)
    {
          // DESCRIPCION: GUARDAR DOCUMENTO ACEPTADO

        var objFacturacion = new NE_Facturacion();      // CLASE NEGOCIO FACTURACION
        try
        {
            objFacturacion.GuardarDocumentoObservado(oDocumento, nombrexml, xmlenviado, cdrxml, valorResumen, firma, mensaje, ticket, situacion);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    private static void Extraer(string rutaFE, string Nombre)
    {
         // DESCRIPCION: FUNCION PARA EXTRAER ZIP

        System.IO.Compression.ZipFile.ExtractToDirectory(rutaFE+ EN_Constante.g_const_prefijoXmlCdr_respuesta + Nombre + EN_Constante.g_const_extension_zip, rutaFE);
    }
    ﻿private static void Extraer2(string rutaFE, string Nombre)
    {
         // DESCRIPCION: FUNCION PARA EXTRAER ZIP CON ZIPSTORER

        ZipStorer zip = ZipStorer.Open(rutaFE + EN_Constante.g_const_prefijoXmlCdr_respuesta + Nombre + EN_Constante.g_const_extension_zip, FileAccess.Read);
        List<ZipStorer.ZipFileEntry> dir = zip.ReadCentralDir();
        string path__1 = rutaFE + EN_Constante.g_const_prefijoXmlCdr_respuesta + Nombre + EN_Constante.g_const_extension_xml;
        bool result;
        foreach (ZipStorer.ZipFileEntry entry in dir)
            result = zip.ExtractFile(entry, path__1);
        zip.Close();
    }


    public bool EnviarXmlResumenElectronico(EN_Resumen oResumen, List<EN_DetalleResumen> oDetResumen, EN_Certificado oCertificado, EN_ComprobanteSunat oCompSunat, string ruta, string nombre, string vResumen, string vFirma, ref string estadoCDR, ref string msjCDR,string produccion)
    {

        //DESCRIPCION: FUNCION PARA ENVIAR XML DE RESUMEN ELECTRONICO A SUNAT

        string xmlenviado = EN_Constante.g_const_vacio;                             // xmlenviado
        string cdrXML = EN_Constante.g_const_vacio;                                 // cdr de xml
        string ticket = EN_Constante.g_const_vacio;                                 // ticket
        string coderror = EN_Constante.g_const_vacio;                               // código de error
        string rutaXml=EN_Constante.g_const_vacio;                                  // ruta xml
        string rutaCdr=EN_Constante.g_const_vacio;                                  // ruta de CDR

        NE_Facturacion objFact = new NE_Facturacion();      // clase de negocio facturación
        var oDocumento = new EN_Documento();                // clase de entidad documento

        try
        {
             //COMPROBAMOS CARPETAS Y ARCHIVOS INICIALES  DE S3 Y LOCAL TEMPORAL
            AppConfiguracion.FuncionesS3.InitialS3(oResumen.Nrodocumento);


            rutaXml=ruta+EN_Constante.g_const_rutaSufijo_xml;
            rutaCdr=ruta+EN_Constante.g_const_rutaSufijo_cdr;

               // CREAMOS TODOS LOS DIRECTORIOS NECESARIOS. SI NO EXISTE
            if ((!System.IO.Directory.Exists(rutaXml)))
                System.IO.Directory.CreateDirectory(rutaXml);

            if ((!System.IO.Directory.Exists(rutaCdr)))
                System.IO.Directory.CreateDirectory(rutaCdr);
            
            // COPIAMOS EL XML DE S3 A LOCAL
            string s3RutaFile   = string.Format("/{0}/{1}",oResumen.Nrodocumento, EN_Constante.g_const_rutaSufijo_xml+ nombre.Trim() + EN_Constante.g_const_extension_xml);
            string localRutaFile= rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml;
            AppConfiguracion.FuncionesS3.DownloadFile(s3RutaFile,localRutaFile).Wait();
  

            // Validamos si existen zipeados anteriores y eliminamos
            if (File.Exists(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_zip))
                File.Delete(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_zip);
            if (File.Exists(rutaCdr.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip))
                File.Delete(rutaCdr.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip);

            // Guardamos el XML en ZIP
            using (var zip = new Ionic.Zip.ZipFile(Encoding.UTF8))
            {
                zip.AddFile(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml, EN_Constante.g_const_vacio);
                zip.Save(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_zip);
            }
            // Enviamos el zip a SUNAT
            xmlenviado = EnviarResumenXML(oCertificado, ruta, nombre, oCompSunat.codigoEtiquetaErrorRes, oResumen.FechaGeneraResumen, ref cdrXML, ref estadoCDR, ref msjCDR,ref  ticket, ref coderror, produccion);
            if (estadoCDR.Trim() == EN_Constante.g_const_situacion_error || estadoCDR.Trim() == EN_Constante.g_const_situacion_rechazado)
            {
                // Guardamos el error en un LOG --- Error al Enviar XML
                if (estadoCDR == EN_Constante.g_const_situacion_error)
                {
                    // Actualizamos el estado de la baja a Error y guardamos el mensaje de error en BD
                    GuardarResumenRechazadaError(oResumen, nombre, xmlenviado, cdrXML, vResumen, vFirma, msjCDR, EN_Constante.g_const_situacion_error, ticket);
                    return false;
                }
                else
                {
                    // Actualizamos el estado del documento a Rechazado y guardamos el mensaje de error en BD
                    GuardarResumenRechazadaError(oResumen, nombre, xmlenviado, cdrXML, vResumen, vFirma, msjCDR, EN_Constante.g_const_situacion_rechazado, ticket);
                    // Validamos el codigo del error 2223 - Si indica que fue presentado anteriormente se debe actualizar a Aceptado las Boletas
                    if (Strings.Trim(coderror) == "2282")
                    {
                        // Actualizamos los comprobantes contenidos en el resumen a Aceptado
                        foreach (EN_DetalleResumen detalleitem in oDetResumen)
                        {
                            oDocumento.Idempresa = oResumen.Empresa_id;
                            oDocumento.Id = detalleitem.Tipodocumentoid + detalleitem.Serie + "-" + detalleitem.Correlativo;
                            oDocumento.Idtipodocumento = detalleitem.Tipodocumentoid;
                            if (detalleitem.Condicion.ToString() == "1")
                                objFact.ActualizarSituacion(oDocumento, EN_Constante.g_const_situacion_aceptado);
                            else if (detalleitem.Condicion.ToString() == "3")
                                objFact.ActualizarSituacion(oDocumento, EN_Constante.g_const_situacion_baja);
                            else
                                objFact.ActualizarSituacion(oDocumento, EN_Constante.g_const_situacion_aceptado);
                        }
                    }
                    return true;
                }
            }
            else
            {
                if (cdrXML.Trim() != EN_Constante.g_const_vacio)
                {
                    // Actualizamos el estado de la Resumen a Aceptada y guardamos el cdrXML BD
                    GuardarResumenAceptada(oResumen, nombre, xmlenviado, cdrXML, vResumen, vFirma, msjCDR, ticket);

                    // Actualizamos los comprobantes contenidos en el resumen a Aceptado
                    foreach (EN_DetalleResumen detalleitem in oDetResumen)
                    {
                        oDocumento.Idempresa = oResumen.Empresa_id;
                        oDocumento.Id = detalleitem.Tipodocumentoid + detalleitem.Serie + "-" + detalleitem.Correlativo;
                        oDocumento.Idtipodocumento = detalleitem.Tipodocumentoid;
                        if (detalleitem.Condicion.ToString() == "1")
                            objFact.ActualizarSituacion(oDocumento, EN_Constante.g_const_situacion_aceptado);
                        else if (detalleitem.Condicion.ToString() == "3")
                            objFact.ActualizarSituacion(oDocumento, EN_Constante.g_const_situacion_baja);
                        else
                            objFact.ActualizarSituacion(oDocumento, EN_Constante.g_const_situacion_aceptado);
                    }
                }
                return true;
            }
        }
        catch (Exception ex)
        {
            msjCDR = msjCDR+" - Error: " + ex.Message;
            GuardarResumenRechazadaError(oResumen, nombre, "No pudo generarse.", "No pudo generarse.", "-", "-", "Error: " + ex.Message, EN_Constante.g_const_situacion_error, ticket);
            return false;
        }
        finally
        {
                //COPIAMOS XML A S3 Y ELIMINAMOS DE TEMPORAL
            AppConfiguracion.FuncionesS3.CopyS3DeleteLocal(oResumen.Nrodocumento,  EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos,  EN_Constante.g_const_rutaSufijo_cdr, EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim(),  EN_Constante.g_const_extension_xml);
            
            //Eliminamos el xml copiado del s3
            if (File.Exists(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml))
            {
                File.Delete(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml);
            }

            // Eliminamos los ZIP
            if (File.Exists(ruta.Trim() +EN_Constante.g_const_rutaSufijo_xml+ nombre.Trim() + EN_Constante.g_const_extension_zip))
                File.Delete(ruta.Trim() +EN_Constante.g_const_rutaSufijo_xml+ nombre.Trim() + EN_Constante.g_const_extension_zip);
            if (File.Exists(ruta.Trim() +EN_Constante.g_const_rutaSufijo_cdr+ EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip))
                File.Delete(ruta.Trim() +EN_Constante.g_const_rutaSufijo_cdr+ EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip);
        }
    }


    public bool EnviarXmlBajasElectronico(EN_Baja oBaja, List<EN_DetalleBaja> oDetBaja, EN_Certificado oCertificado, EN_ComprobanteSunat oCompSunat, string ruta, string nombre, string vResumen, string vFirma, ref string estadoCDR, ref string msjCDR, string produccion)
    {
        string xmlenviado = EN_Constante.g_const_vacio;                             // xml enviado
        string cdrXML = EN_Constante.g_const_vacio;                                 // xml del cdr
        string ticket = EN_Constante.g_const_vacio;                                 // ticket
        string coderror = EN_Constante.g_const_vacio;                               // codigo de error
         string rutaXml=EN_Constante.g_const_vacio;                                 // ruta xml
        string rutaCdr=EN_Constante.g_const_vacio;                                  // ruta de CDR
        try
        {
             //COMPROBAMOS CARPETAS Y ARCHIVOS INICIALES  DE S3 Y LOCAL TEMPORAL
            AppConfiguracion.FuncionesS3.InitialS3(oBaja.Nrodocumento);

            rutaXml=ruta+EN_Constante.g_const_rutaSufijo_xml;
            rutaCdr=ruta+EN_Constante.g_const_rutaSufijo_cdr;

            // CREAMOS TODOS LOS DIRECTORIOS NECESARIOS. SI NO EXISTE
            if ((!System.IO.Directory.Exists(rutaXml)))
                System.IO.Directory.CreateDirectory(rutaXml);

            if ((!System.IO.Directory.Exists(rutaCdr)))
                System.IO.Directory.CreateDirectory(rutaCdr);
            
            // COPIAMOS EL XML DE S3 A LOCAL
            string s3RutaFile   = string.Format("/{0}/{1}",oBaja.Nrodocumento, EN_Constante.g_const_rutaSufijo_xml+ nombre.Trim() + EN_Constante.g_const_extension_xml);
            string localRutaFile= rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml;
            AppConfiguracion.FuncionesS3.DownloadFile(s3RutaFile,localRutaFile).Wait();
  



            // Validamos si existen zipeados anteriores y eliminamos
            if (File.Exists(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_zip))
            {
                File.Delete(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_zip);
            }

            if (File.Exists(rutaCdr.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip))
            {
                File.Delete(rutaCdr.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip);
            }

            // Guardamos el XML en ZIP
            using (var zip = new  Ionic.Zip.ZipFile(Encoding.UTF8))
            {
                zip.AddFile(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml, EN_Constante.g_const_vacio);
                zip.Save(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_zip);
            }
            // Enviamos el zip a SUNAT
            xmlenviado = EnviarResumenXML(oCertificado, ruta, nombre, oCompSunat.codigoEtiquetaErrorRes, oBaja.FechaComunicacion,ref cdrXML, ref estadoCDR, ref msjCDR, ref ticket, ref coderror , produccion);
            if (estadoCDR.Trim() == EN_Constante.g_const_situacion_error || estadoCDR.Trim() == EN_Constante.g_const_situacion_rechazado)
            {
                // Guardamos el error en un LOG --- Error al Enviar XML
                if (estadoCDR.Trim() == EN_Constante.g_const_situacion_error)
                {
                    // Actualizamos el estado de la baja a Error y guardamos el mensaje de error en BD
                    GuardarBajaRechazadaError(oBaja, nombre, xmlenviado, cdrXML, vResumen, vFirma, msjCDR, EN_Constante.g_const_estado_error, ticket);
                    return false;
                }
                else
                {
                    // Actualizamos el estado del documento a Rechazado y guardamos el mensaje de error en BD
                    GuardarBajaRechazadaError(oBaja, nombre, xmlenviado, cdrXML, vResumen, vFirma, msjCDR, EN_Constante.g_const_estado_rechazada, ticket);
                    return true;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(cdrXML.Trim()))
                {
                    // Actualizamos el estado de la baja a Aceptada y guardamos el cdrXML BD
                    GuardarBajaAceptada(oBaja, nombre, xmlenviado, cdrXML, vResumen, vFirma, msjCDR, ticket);
                    // Actualizamos los comprobantes contenidos en el resumen a Aceptado
                    foreach (EN_DetalleBaja detalleitem in oDetBaja)
                    {
                        var objFact = new NE_Facturacion();
                        var oDocumento = new EN_Documento();
                        oDocumento.Idempresa = oBaja.Empresa_id;
                        oDocumento.Id = detalleitem.Tipodocumentoid + detalleitem.Serie + "-" + detalleitem.Numero;
                        oDocumento.Idtipodocumento = detalleitem.Tipodocumentoid;
                        objFact.ActualizarSituacion(oDocumento, "B");
                    }
                }

                return true;
            }
        }
        catch (Exception ex)
        {
            msjCDR = msjCDR +" - Error: " + ex.Message;
            GuardarBajaRechazadaError(oBaja, nombre, "No pudo generarse.", "No pudo generarse.", "-", "-", "Error: " + ex.Message, "E", ticket);
            return false;
        }
        finally
        {

            //COPIAMOS XML A S3 Y ELIMINAMOS DE TEMPORAL
            AppConfiguracion.FuncionesS3.CopyS3DeleteLocal(oBaja.Nrodocumento,  EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos,  EN_Constante.g_const_rutaSufijo_cdr, EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim(),  EN_Constante.g_const_extension_xml);
            
            //Eliminamos el xml copiado del s3
            if (File.Exists(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml))
            {
                File.Delete(rutaXml.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml);
            }


            // Eliminamos los ZIP
            if (File.Exists(ruta.Trim() + EN_Constante.g_const_rutaSufijo_xml +  nombre.Trim() +  EN_Constante.g_const_extension_zip))
            {
                File.Delete(ruta.Trim() +EN_Constante.g_const_rutaSufijo_xml +  nombre.Trim() + EN_Constante.g_const_extension_zip);
            }

            if (File.Exists(ruta.Trim() + EN_Constante.g_const_rutaSufijo_cdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip))
            {
                File.Delete(ruta.Trim() + EN_Constante.g_const_rutaSufijo_cdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip);
            }
        }
    }


    private static string EnviarResumenXML(EN_Certificado oCertificado, string rutaFE, string Nombre, string codEtiqueta, string fechadoc, ref string cdrXML, ref string estadoError, ref string msjError, ref string ticket, ref string codigoerror, string produccion)
    {
        //DESCRIPCION: FUNCION PARA CONECTARSE A WEB API DE ENVIO A SUNAT 
       

        NE_Facturacion TablaFEADObj = new NE_Facturacion();         // CLASE NEGOCIO FACTURACION

        string rutaXml=rutaFE+EN_Constante.g_const_rutaSufijo_xml;               // RUTA XML
        string rutaCdr=rutaFE+EN_Constante.g_const_rutaSufijo_cdr;               // RUTA CDR
        string cadenaXMLEnv = EN_Constante.g_const_vacio;                   // CADENA XML ENVIADO
        XmlDocument xmlEnv = new XmlDocument();     // XML DE DOCUMENTO
        xmlEnv.PreserveWhitespace = false;
        int segundosEsperaRes= EN_Constante.g_const_0;               // SEGUNDOS DE ESPERA PARA CONSULTAR TICKET EN RESUMEN
        NE_Proceso objProc = new NE_Proceso();                  //clase negocio proceso
        EN_Parametro res_par = new EN_Parametro();              // clase entidad parametro para resultado
        EN_Parametro bus_par = new EN_Parametro();              // clase entidad parametro para búsqueda
       

        using (FileStream fs = new FileStream(rutaXml + Nombre + EN_Constante.g_const_extension_xml, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        {
            xmlEnv.Load(fs);
        }

        cadenaXMLEnv = xmlEnv.OuterXml.ToString().Trim();

        // Para validar CDR
        string descripcionerror = EN_Constante.g_const_vacio;
        FileStream fsRpta = new FileStream(rutaCdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + Nombre + EN_Constante.g_const_extension_zip, FileMode.Create);
        try
        {

            bus_par.par_conceptopfij=EN_Constante.g_const_1;
            bus_par.par_conceptocorr=EN_Constante.g_const_7;
            res_par =objProc.buscar_tablaParametro(bus_par);
            segundosEsperaRes= res_par.par_tipo;

            System.Net.ServicePointManager.UseNagleAlgorithm = true;
            System.Net.ServicePointManager.Expect100Continue = false;
            System.Net.ServicePointManager.CheckCertificateRevocationList = true;

            XmlDocument xmlCdr = new XmlDocument();
            string rutaEndpoint;
            dynamic wsEnd = null;

            if (oCertificado.flagOSE == EN_Constante.g_const_1.ToString())
            {
                // if(produccion==EN_Constante.g_const_si) rutaEndpoint=EN_ConfigConstantes.Instance.const_ProdNubefact;
                // else rutaEndpoint= EN_ConfigConstantes.Instance.const_BetaNubefact;
                // wsEnd= new ServicioNubeFact.billServiceClient.EndpointConfiguration();
                // ServicioNubeFact.billServiceClient ws = new ServicioNubeFact.billServiceClient(wsEnd, rutaEndpoint);
                // ServicioNubeFact.sendSummaryRequest envio = new ServicioNubeFact.sendSummaryRequest();
                // // ServicioNubeFact.sendSummaryResponse datos = new ServicioNubeFact.sendSummaryResponse();
                // ServicioNubeFact.statusResponse status = new ServicioNubeFact.statusResponse();

                // ws.ClientCredentials.UserName.UserName = oCertificado.UserName;
                // ws.ClientCredentials.UserName.Password = oCertificado.Clave;

                // var behavior = new PasswordDigestBehavior(oCertificado.UserName, oCertificado.Clave);
                // ws.Endpoint.EndpointBehaviors.Add(behavior);

                // envio.fileName = Nombre + EN_Constante.g_const_extension_zip;
                // envio.contentFile =File.ReadAllBytes(rutaXml + Nombre + EN_Constante.g_const_extension_zip);

                // // Grabar Trama de Envío
                // System.Collections.ArrayList arraylog = new System.Collections.ArrayList();
                // arraylog.Add(config.Serializar(envio.GetType(), envio));
                // config.GrabarTramaLog(arraylog, "sendSummaryRequest", "EnviarResumenXML",Strings.Format(Conversions.ToDate(fechadoc), "yyyyMMdd") , Nombre, rutaFE);

                // // ws.Open();
                // dynamic datos= null;
                // datos.ticket = ws.sendSummaryAsync(envio.fileName, envio.contentFile, EN_Constante.g_const_vacio);
                // ticket = datos.ticket;
                // Logs.SaveInfoLog<NE_Proceso>("GenerarXML - EnviarResumenXML: Genera Correctamente Ticket:" + ticket);

                // // Grabar Trama de Respuesta
                // System.Collections.ArrayList arraylog2 = new System.Collections.ArrayList();
                // arraylog2.Add(config.Serializar(datos.GetType, datos));
                // config.GrabarTramaLog(arraylog2, "sendSummaryResponse", "EnviarResumenXML",Strings.Format(Conversions.ToDate(fechadoc), "yyyyMMdd")  , Nombre, rutaFE);

                // status = ws.getStatusAsync(datos.ticket);
                // Logs.SaveInfoLog<NE_Proceso>("GenerarXML - EnviarResumenXML: Obtiene datos del ticket");

                // fsRpta.Write(status.content, 0, status.content.Length);
                // fsRpta.Close();

                // Extraer2(rutaCdr, Nombre);
                // xmlCdr.PreserveWhitespace = false;
                // xmlCdr.Load(rutaCdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + Nombre + EN_Constante.g_const_extension_xml);
                // cdrXML = xmlCdr.OuterXml.ToString().Trim();
                // VerificarNBOSECDR(xmlCdr.DocumentElement, codEtiqueta, msjError, codigoerror);
            }
            else if (oCertificado.flagOSE == EN_Constante.g_const_2.ToString())
            {
//                  if(produccion==EN_Constante.g_const_si) rutaEndpoint=EN_ConfigConstantes.Instance.const_ProdEfac;
//                 else rutaEndpoint= EN_ConfigConstantes.Instance.const_BetaEfac;
                
//                 wsEnd= new ServicioEfact.BillServiceClient.EndpointConfiguration();

//                 ServicioEfact.BillServiceClient ws = new ServicioEfact.BillServiceClient(wsEnd, rutaEndpoint);
//                 ServicioEfact.sendSummaryRequest envio = new ServicioEfact.sendSummaryRequest();
//                 // ServicioEfact.sendSummaryResponse datos = new ServicioEfact.sendSummaryResponse();
//                 ServicioEfact.statusResponse status = new ServicioEfact.statusResponse();

//                 ws.ClientCredentials.UserName.UserName = oCertificado.UserName;
//                 ws.ClientCredentials.UserName.Password = oCertificado.Clave;

//                 var behavior = new PasswordDigestBehavior(oCertificado.UserName, oCertificado.Clave);
//                 ws.Endpoint.EndpointBehaviors.Add(behavior);

//                 envio.fileName = Nombre + EN_Constante.g_const_extension_zip;
//                 envio.contentFile = File.ReadAllBytes(rutaXml + Nombre + EN_Constante.g_const_extension_zip);

//                 // Grabar Trama de Envío
//                 System.Collections.ArrayList arraylog = new System.Collections.ArrayList();
//                 arraylog.Add(config.Serializar(envio.GetType(), envio));
//                 config.GrabarTramaLog(arraylog, "sendSummaryRequest", "EnviarResumenXML",  Strings.Format(Conversions.ToDate(fechadoc), "yyyyMMdd") , Nombre, rutaFE);

//                 // ws.Open();
//                 dynamic datos=null
// ;                datos = ws.sendSummaryAsync(envio.fileName, envio.contentFile);
//                 ticket = datos.ticket;
//                 Logs.SaveInfoLog<NE_Proceso>("GenerarXML - EnviarResumenXML: Genera Correctamente Ticket:" + ticket);

//                 // Grabar Trama de Respuesta
//                 System.Collections.ArrayList arraylog2 = new System.Collections.ArrayList();
//                 arraylog2.Add(config.Serializar(datos.GetType(), datos));
//                 config.GrabarTramaLog(arraylog2, "sendSummaryResponse", "EnviarResumenXML", Strings.Format(Conversions.ToDate(fechadoc), "yyyyMMdd"), Nombre, rutaFE);

//                 status = ws.getStatusAsync(datos.ticket);
//                 Logs.SaveInfoLog<NE_Proceso>("GenerarXML - EnviarResumenXML: Obtiene datos del ticket");

//                 fsRpta.Write(status.content, 0, status.content.Length);
//                 fsRpta.Close();

//                 Extraer2(rutaCdr, Nombre);
//                 xmlCdr.PreserveWhitespace = false;
//                 xmlCdr.Load(rutaCdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + Nombre + EN_Constante.g_const_extension_xml);
//                 cdrXML = xmlCdr.OuterXml.ToString().Trim();
                // VerificarEFOSECDR(xmlCdr.DocumentElement, codEtiqueta, msjError, codigoerror);
            }
            else
            {
                 if(produccion==EN_Constante.g_const_si) rutaEndpoint=EN_ConfigConstantes.Instance.const_ProdSunatDoc;
                else rutaEndpoint= EN_ConfigConstantes.Instance.const_BetaSunatDoc;
              
                wsEnd= new ServicioSunat.billServiceClient.EndpointConfiguration();
                ServicioSunat.billServiceClient ws = new ServicioSunat.billServiceClient(wsEnd, rutaEndpoint);
                ServicioSunat.sendSummaryRequest envio = new ServicioSunat.sendSummaryRequest();

                ws.ClientCredentials.UserName.UserName = oCertificado.UserName;
                ws.ClientCredentials.UserName.Password = oCertificado.Clave;

                var behavior = new PasswordDigestBehavior(oCertificado.UserName, oCertificado.Clave);
                ws.Endpoint.EndpointBehaviors.Add(behavior);

                envio.fileName = Nombre + EN_Constante.g_const_extension_zip;
                envio.contentFile = File.ReadAllBytes(rutaXml + Nombre + EN_Constante.g_const_extension_zip);

             
                dynamic datos = null;
                dynamic status= null;
                datos = ws.sendSummaryAsync(envio.fileName, envio.contentFile,EN_Constante.g_const_vacio);
                var datosR= datos.Result;
                ticket = datosR.ticket;

                // timer para consultar ticket
                System.Threading.Thread.Sleep(TimeSpan.FromSeconds(segundosEsperaRes));

                status = ws.getStatusAsync(ticket);
                var statusR = status.Result.status;
                     ws.Close();

                fsRpta.Write(statusR.content, 0, statusR.content.Length);
                fsRpta.Close();

            

                Extraer2(rutaCdr, Nombre);
                xmlCdr.PreserveWhitespace = false;
                xmlCdr.Load(rutaCdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + Nombre + EN_Constante.g_const_extension_xml);
                cdrXML = xmlCdr.OuterXml.ToString().Trim();
                VerificarSunatCDR(xmlCdr.DocumentElement, codEtiqueta, ref msjError, ref codigoerror);
            }

         
            if (codigoerror != EN_Constante.g_const_vacio)
            {
                estadoError = TablaFEADObj.ObtenerCodigoAlternoPorCodigoSistema("999", codigoerror);
                descripcionerror = TablaFEADObj.ObtenerDescripcionPorCodElemento("999", codigoerror);
                if (estadoError == EN_Constante.g_const_situacion_rechazado)
                    msjError = "Resumen rechazado: " + codigoerror + "-" + descripcionerror;
                else if (estadoError == EN_Constante.g_const_situacion_error)
                    msjError = "Resumen con excepcion: " + codigoerror + "-" + descripcionerror;
                else if (estadoError == EN_Constante.g_const_situacion_observado)
                    msjError = "Resumen observado: " + codigoerror + "-" + descripcionerror;
                else if (System.Convert.ToInt32(codigoerror) >= 100 & System.Convert.ToInt32(codigoerror) <= 1999)
                {
                    estadoError = EN_Constante.g_const_situacion_error;
                    msjError = "Resumen con excepcion " + "-" + codigoerror;
                }
                else if (System.Convert.ToInt32(codigoerror) >= 2000 & System.Convert.ToInt32(codigoerror) <= 3999)
                {
                    estadoError = EN_Constante.g_const_situacion_rechazado;
                    msjError = "Resumen rechazado " + "-" + codigoerror;
                }
                else if (System.Convert.ToInt32(codigoerror) >= 4000)
                {
                    estadoError = EN_Constante.g_const_situacion_observado;
                    msjError = "Resumen obervado " + "-" + codigoerror;
                }
                else
                {
                    estadoError = EN_Constante.g_const_situacion_error;
                    msjError = "Error al enviar Resumen a la SUNAT " + "-" + codigoerror;
                }
            }
            else
                estadoError = EN_Constante.g_const_situacion_aceptado;
        }
        catch (System.ServiceModel.FaultException ex)
        {
            codigoerror = ex.Code.Name.ToString().Replace(codEtiqueta, EN_Constante.g_const_vacio).Trim();
            estadoError = TablaFEADObj.ObtenerCodigoAlternoPorCodigoSistema("999", codigoerror);
            descripcionerror = TablaFEADObj.ObtenerDescripcionPorCodElemento("999", codigoerror);
            if (estadoError.Trim() == EN_Constante.g_const_vacio & Information.IsNumeric(ex.Message))
            {
                codigoerror = Configuracion.CompletarCeros((ex.Message), 4);
                estadoError = TablaFEADObj.ObtenerCodigoAlternoPorCodigoSistema("999", codigoerror);
                descripcionerror = TablaFEADObj.ObtenerDescripcionPorCodElemento("999", codigoerror);
            }
            if (estadoError == EN_Constante.g_const_situacion_rechazado)
                msjError = "Resumen rechazado: " + codigoerror + "-" + descripcionerror;
            else if (estadoError == EN_Constante.g_const_situacion_error)
                msjError = "Resumen con excepción: " + codigoerror + "-" + descripcionerror;
            else if (estadoError == EN_Constante.g_const_situacion_observado)
                msjError = "Resumen observado: " + codigoerror + "-" + descripcionerror;
            else if (System.Convert.ToInt32(codigoerror) >= 100 & System.Convert.ToInt32(codigoerror) <= 1999)
            {
                estadoError = EN_Constante.g_const_situacion_error;
                msjError = "Resumen con excepcion " + "-" + codigoerror;
            }
            else if (System.Convert.ToInt32(codigoerror) >= 2000 & System.Convert.ToInt32(codigoerror) <= 3999)
            {
                estadoError = EN_Constante.g_const_situacion_rechazado;
                msjError = "Resumen rechazado " + "-" + codigoerror;
            }
            else if (System.Convert.ToInt32(codigoerror) >= 4000)
            {
                estadoError = EN_Constante.g_const_situacion_observado;
                msjError = "Resumen obervado " + "-" + codigoerror;
            }
            else
            {
                estadoError = EN_Constante.g_const_situacion_error;
                msjError = "Error al enviar Resumen a la SUNAT " + ex.Code.Name.ToString() + "-" + ex.Message.Trim() + "-" + codigoerror;
            }
        }
        catch (Exception ex)
        {
            estadoError = EN_Constante.g_const_situacion_error;
            msjError = "Error al enviar Resumen a la SUNAT: " + ex.Message.ToString();
        }
        finally
        {
            fsRpta.Close();
        }
        return cadenaXMLEnv;
    }
  
    public static void GuardarResumenAceptada(EN_Resumen oResumen, string nombrexml, string xmlbase64, string cdrxml, string valorResumen, string firma, string mensaje, string ticket)
    {
        // DESCRIPCION: GUARDAR RESUMEN ACEPTADA

        NE_Resumen objResumen = new NE_Resumen();           // CLASE NEGOCIO RESUMEN
        try
        {
            objResumen.GuardarResumenAceptada(oResumen, nombrexml, xmlbase64, cdrxml, valorResumen, firma, mensaje, ticket);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }
    public static void GuardarResumenRechazadaError(EN_Resumen oResumen, string nombrexml, string xmlbase64, string cdrxml, string valorResumen, string firma, string errortext, string estado, string ticket)
    {
        // DESCRIPCION: GUARDAR RESUMEN RECHAZADA ERROR

        NE_Resumen objResumen = new NE_Resumen();            // CLASE NEGOCIO RESUMEN
        try
        {
            objResumen.GuardarResumenRechazadaError(oResumen, nombrexml, xmlbase64, cdrxml, valorResumen, firma, errortext, estado, ticket);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

     public void GuardarBajaAceptada(EN_Baja oBaja, string nombrexml, string xmlbase64, string cdrxml, string valorResumen, string firma, string mensaje, string ticket)
    {
        var objBaja = new NE_Baja();
        try
        {
            objBaja.GuardarBajaAceptada(oBaja, nombrexml, xmlbase64, cdrxml, valorResumen, firma, mensaje, ticket);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void GuardarBajaRechazadaError(EN_Baja oBaja, string nombrexml, string xmlbase64, string cdrxml, string valorResumen, string firma, string errortext, string estado, string ticket)
    {
        var objBaja = new NE_Baja();
        try
        {
            objBaja.GuardarBajaRechazadaError(oBaja, nombrexml, xmlbase64, cdrxml, valorResumen, firma, errortext, estado, ticket);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }




   


    
    }
    
}
