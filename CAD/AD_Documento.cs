﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_Documento.cs
 VERSION : 1.0
 OBJETIVO: Clase de acceso a datos documento
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using Microsoft.VisualBasic;
using CEN;
using System.Data.SqlClient;
using System.Data;
using Microsoft.VisualBasic.CompilerServices;

namespace CAD
{

        public partial class AD_Documento
        {
            private readonly AD_Cado _datosConexion;        // CLASE DE CONEXION CADO
            private readonly SqlConnection _sqlConexion;    // CONEXION SQL
            private readonly SqlCommand _sqlCommand;        // COMANDO SQL

            public AD_Documento()
            {
                _datosConexion = new AD_Cado();
                _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
                _sqlCommand = new SqlCommand();
            }

          

            public string consultarSituacion(EN_Documento pDocumento)
            {
                // DESCRIPCION: CONSULTAR SITUACION
                
                string Situacion = "";          // SITUACION
                SqlCommand cmd ;                // COMANDO SQL
                SqlDataReader Rs;               // DATA READER SQL 
                
                try
                {
                    
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_consultarSituacion",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id",   pDocumento.Id.Trim());
                    cmd.Parameters.AddWithValue("@Idempresa", pDocumento.Idempresa);
                    cmd.Parameters.AddWithValue("@Idtipodocumento", pDocumento.Idtipodocumento.Trim());
                    Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                            Situacion = Conversions.ToString(Interaction.IIf(Information.IsDBNull(Rs["situacion"]), "", Rs["situacion"]));
                    }
                    else
                    {
                        Situacion = "";
                    }

                    Rs.Close();
                    return Situacion.Trim();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

        public EN_Resumenfirma GetResumenFirma(EN_Documento oDocumento)
        {
            // DESCRIPCION: RESUMEN FIRMA

            EN_Resumenfirma oRF = new EN_Resumenfirma();        // CLASE ENTIDAD RESUMENT FIRMA
            SqlCommand cmd;                                     // COMANDO SQL    
            SqlDataReader Rs;                                   // DATA READER SQL
                try
                {
                
            
             
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_devresumenfirma",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                     
                    cmd.Parameters.AddWithValue("@Id",  oDocumento.Id.Trim());
                    cmd.Parameters.AddWithValue("@Idempresa", oDocumento.Idempresa);
                    cmd.Parameters.AddWithValue("@Idtipodocumento", oDocumento.Idtipodocumento.Trim());

                    Rs = cmd.ExecuteReader();

                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        oRF.resumen= (string)Rs["resumen"];
                        oRF.firma = (string)Rs["firma"];
                    }
                }
                Rs.Close();
                return oRF;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }

        }

        public EN_Resumenfirma GetResumenFirmaRD(EN_Resumen oDocumento)
        {
                // DESCRIPCION: RESUMEN FIRMA RESUMEN DIARIO

                EN_Resumenfirma oRF = new EN_Resumenfirma();        // CLASE ENTIDAD RESUMENT FIRMA
                SqlCommand cmd;                                     // COMANDO SQL    
                SqlDataReader Rs;                                   // DATA READER SQL
               
                try
                {
            
             
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_devresumenfirmaRD",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                     
                    cmd.Parameters.AddWithValue("@Id",  oDocumento.Id);
                    cmd.Parameters.AddWithValue("@Idempresa", oDocumento.Empresa_id);
                    cmd.Parameters.AddWithValue("@Idtipodocumento", oDocumento.Tiporesumen.Trim());

                    Rs = cmd.ExecuteReader();

                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        oRF.resumen= (string)Rs["resumen"];
                        oRF.firma = (string)Rs["firma"];
                    }
                }
                Rs.Close();
                return oRF;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }

        }
        
        public EN_Resumenfirma GetResumenFirmaCB(BajaSunat oDocumento)
        {
                // DESCRIPCION: RESUMEN FIRMA RESUMEN DIARIO

                EN_Resumenfirma oRF = new EN_Resumenfirma();        // CLASE ENTIDAD RESUMENT FIRMA
                SqlCommand cmd;                                     // COMANDO SQL    
                SqlDataReader Rs;                                   // DATA READER SQL
               
                try
                {
            
             
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_devresumenfirmaCB",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                     
                    cmd.Parameters.AddWithValue("@Id",  oDocumento.Id);
                    cmd.Parameters.AddWithValue("@Idempresa", oDocumento.Empresa_id);
                    cmd.Parameters.AddWithValue("@Idtipodocumento", oDocumento.Tiporesumen.Trim());

                    Rs = cmd.ExecuteReader();

                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        oRF.resumen= (string)Rs["resumen"];
                        oRF.firma = (string)Rs["firma"];
                    }
                }
                Rs.Close();
                return oRF;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }

        }


        }
    

}
