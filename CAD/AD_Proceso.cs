﻿using System;
using System.Collections.Generic;

using Microsoft.VisualBasic;
using CEN;
using System.Data.SqlClient;

using System.Data;

namespace CAD
{


    public class AD_Proceso
    {
        private readonly AD_Cado _datosConexion;
        private readonly SqlConnection _sqlConexion;
        private readonly SqlCommand _sqlCommand;
         private string TiempoEspera {get;}
        public AD_Proceso()
        {
            _datosConexion = new AD_Cado();
            _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
            _sqlCommand = new SqlCommand();
             TiempoEspera=EN_ConfigConstantes.Instance.const_TiempoEspera;
        }
       




        public EN_Parametro buscar_tablaParametro(EN_Parametro par_busqueda)
        {
            //DESCRIPCION: NUMERO DE DIAS PERMITIDOS PARA ENVIAR A SUNAT

            SqlCommand cmd; // comando para certificado
            SqlDataReader Rs;   //Data Reader para certificado
            EN_Parametro res_par = new EN_Parametro();

            try
            {
                _sqlConexion.Open();
                cmd = new SqlCommand("Usp_buscar_parametroGeneral",_sqlConexion);
                 
    
                cmd.CommandType= CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@par_concepto",  par_busqueda.par_conceptopfij);
                cmd.Parameters.AddWithValue("@par_correlativo", par_busqueda.par_conceptocorr);
                cmd.Parameters.AddWithValue("@p_par_descripcion", par_busqueda.par_descripcion);
                cmd.Parameters.AddWithValue("@p_par_descripcion2",  par_busqueda.par_descripcion2 );
                cmd.Parameters.AddWithValue("@p_par_int1",  par_busqueda.par_int1 );
                cmd.Parameters.AddWithValue("@p_par_int2", par_busqueda.par_int2 );
                cmd.Parameters.AddWithValue("@p_par_tipo", par_busqueda.par_tipo );
                Rs = cmd.ExecuteReader();

                if(Rs.HasRows)
                {
                    while(Rs.Read())
                    {
                       res_par.par_descripcion= (string)Interaction.IIf(Information.IsDBNull(Rs["par_descripcion"]), "", Rs["par_descripcion"]);           
                       res_par.par_descripcion2= (string)Interaction.IIf(Information.IsDBNull(Rs["par_descripcion2"]), "", Rs["par_descripcion2"]);           
                       res_par.par_int1= (int)Interaction.IIf(Information.IsDBNull(Rs["par_int1"]), 0, Rs["par_int1"]);           
                       res_par.par_int2= (int)Interaction.IIf(Information.IsDBNull(Rs["par_int2"]), 0, Rs["par_int2"]);   
                       res_par.par_float1= (decimal)Interaction.IIf(Information.IsDBNull(Rs["par_float1"]),(decimal)0, Rs["par_float1"]);   
                       res_par.par_float2= (decimal)Interaction.IIf(Information.IsDBNull(Rs["par_float2"]), (decimal)0, Rs["par_float2"]);   
                       res_par.par_date1= (string)Interaction.IIf(Information.IsDBNull(Rs["par_date1"]), "", Rs["par_date1"].ToString());   
                       res_par.par_date2= (string)Interaction.IIf(Information.IsDBNull(Rs["par_date1"]), "", Rs["par_date2"].ToString()); 
                       res_par.par_tipo= (int)Interaction.IIf(Information.IsDBNull(Rs["par_tipo"]), 0, Rs["par_tipo"]);   
                    }
                }
                Rs.Close();
                return res_par;

            }
            catch (System.Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }


        
        
        

       
    }

}

