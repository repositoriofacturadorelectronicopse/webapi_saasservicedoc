﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_Facturacion.cs
 VERSION : 1.0
 OBJETIVO: Clase de acceso a datos facturación
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using CEN;
using System.Text;
using System.Collections.Generic;

namespace CAD
{

 
        public partial class AD_Facturacion
        {
            private readonly AD_Cado _datosConexion;        // CLASE CONEXION CADO
            private readonly SqlConnection _sqlConexion;    // CONEXION SQL
            private readonly SqlCommand _sqlCommand;        // COMANDO SQL

            public AD_Facturacion()
            {
                _datosConexion = new AD_Cado();
                _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
                _sqlCommand = new SqlCommand();
            }

             public string buscarParametroAppSettings(int codconcepto, int codcorrelativo)
            {
                // DESCRIPCION: BUSCAR CONSTANTES EN TABLA SUNAT
                
                SqlCommand cmd ; //comando
                SqlDataReader reader ; //Data reader
                try 
                {
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_buscar_parametro",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@flag", 0);
                    cmd.Parameters.AddWithValue("@idBusqueda1", codconcepto);
                    cmd.Parameters.AddWithValue("@idBusqueda2", codcorrelativo);

                    reader = cmd.ExecuteReader();

                    string response="";
                    while(reader.Read())
                    {
                        response = (reader["par_descripcion"]==null)?"":reader["par_descripcion"].ToString();
                    }

                    return response;
                    
                }
                catch (Exception ex)
                {
                     _sqlConexion.Close();
                    
                    throw ex;
                }
                finally {
                    _sqlConexion.Close();
                }
                
            }
            
            public string buscarConstantesTablaSunat(string codtabla,string codalterno)
            {
                // DESCRIPCION: BUSCAR CONSTANTES EN TABLA SUNAT

                SqlCommand cmd ;            //comando
                SqlDataReader reader ;      //Data reader
                int contador=0;             // CONTADOR
                string response="";         // RESPUESTA
                StringBuilder bld ;         // ACUMULADOR DE STRING  
                try 
                {
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_buscar_tabla_codigosunat",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    // flag=0 para buscar por cod alterno, y 1 para codelemento, 2 obtener descripcion y buscar por codsunat
                    // 3= obtienes descripcion y busqueda codelemento, 4 obtienes codalterno y busqueda codelemento

                    cmd.Parameters.AddWithValue("@flag", 0);
                    cmd.Parameters.AddWithValue("@codtabla", codtabla);
                    cmd.Parameters.AddWithValue("@codbusqueda", codalterno);

                    reader = cmd.ExecuteReader();
                    bld = new StringBuilder();

                    while(reader.Read())
                    {
                        if(codalterno=="RG"){
                            if(contador!=0)
                            {
                                bld.Append(",");
                            }
                               bld.Append("'"+reader["descpcampo"].ToString()+"'");
                               contador++;
                        }else{
                              bld.Append(reader["descpcampo"].ToString());
                        }
                    }
                    
                    response = bld.ToString();

                    return response;
                    
                }
                catch (Exception ex)
                {
                     _sqlConexion.Close();
                    
                    throw ex;
                }
                finally {
                    _sqlConexion.Close();
                }

            }

          

            public string ObtenerDescripcionPorCodElemento(string strCodTabla, string strCodSistema)
            {
                // DESCRIPCION: BUSCAR DESCRIPCION POR CODIGO CODIGO ELEMENTO

                 string descripcion = "";       // DESCRIPCION
                 SqlCommand cmd;                // COMANDO
                 SqlDataReader Rs;              // DATA READER

                try
                {
              
                    _sqlConexion.Open();
                        cmd = new SqlCommand("Usp_buscar_tabla_codigosunat",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    // flag=0 para buscar por cod alterno, y 1 para codelemento, 2 obtener descripcion y buscar por codsunat
                    // 3= obtienes descripcion y busqueda codelemento, 4 obtienes codalterno y busqueda codelemento
                    cmd.Parameters.AddWithValue("@flag", 3);
                    cmd.Parameters.AddWithValue("@codtabla",strCodTabla.Trim());
                    cmd.Parameters.AddWithValue("@codbusqueda",strCodSistema.Trim());

                    Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                            descripcion = Conversions.ToString(Rs["descpcampo"]);
                    }

                    Rs.Close();
                    return descripcion.Trim();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

            public string ObtenerCodigoAlternoPorCodigoSistema(string strCodTabla, string strCodSistema)
            {
                // DESCRIPCION: OBTENER CODIGO ALTERNO POR CODIGO DE SISTEMA
                string CodigoSunat = ""; // CODIGO SUNAT
                SqlCommand cmd ; // COMANDO
                SqlDataReader Rs; // DATA READER

                try
                {
                   
             
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_buscar_tabla_codigosunat",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    // flag=0 para buscar por cod alterno, y 1 para codelemento, 2 obtener descripcion y buscar por codsunat
                    // 3= obtienes descripcion y busqueda codelemento, 4 obtienes codalterno y busqueda codelemento
                    cmd.Parameters.AddWithValue("@flag", 4);
                    cmd.Parameters.AddWithValue("@codtabla",strCodTabla.Trim());
                    cmd.Parameters.AddWithValue("@codbusqueda",strCodSistema.Trim());

                    Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                            CodigoSunat = Conversions.ToString(Rs["descpcampo"]);
                    }

                    Rs.Close();
                    return CodigoSunat.Trim();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

    public void GuardarDocumentoAceptado(EN_Documento oDocumento, string nombrexml, string xmlenviado, string cdrxml, string valorResumen, string firma, string mensaje, string ticket)
    {
        // DESCRIPCION: GUARDAR DOCUMENTO ACEPTADO

        string nrocomprobElect = oDocumento.Serie + "-" + oDocumento.Numero;        // NUMERO DE COMPROBANTE ELECTRONICO
        
        try
        {
            
            _sqlConexion.Open();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
            _sqlCommand.Connection = _sqlConexion;
            _sqlCommand.CommandText = "Usp_guardar_facturaelectronica";
            _sqlCommand.Parameters.AddWithValue("@empresa_id", oDocumento.Idempresa);
            _sqlCommand.Parameters.AddWithValue("@documento_id", oDocumento.Id);
            _sqlCommand.Parameters.AddWithValue("@tipodocumento_id", oDocumento.Idtipodocumento);
            _sqlCommand.Parameters.AddWithValue("@numcompelectronico", nrocomprobElect);
            _sqlCommand.Parameters.AddWithValue("@nombrexml", nombrexml);
            _sqlCommand.Parameters.AddWithValue("@xmlenviado", xmlenviado);
            _sqlCommand.Parameters.AddWithValue("@xmlcdr", cdrxml);
            _sqlCommand.Parameters.AddWithValue("@valorresumen", valorResumen);
            _sqlCommand.Parameters.AddWithValue("@firma", firma);
            _sqlCommand.Parameters.AddWithValue("@mensaje", mensaje);
            _sqlCommand.Parameters.AddWithValue("@ticket", ticket);
            
            
            _sqlCommand.ExecuteNonQuery();
            
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }

       public void GuardarDocumentoObservado(EN_Documento oDocumento, string nombrexml, string xmlenviado, string cdrxml, string valorResumen, string firma, string mensaje, string ticket, string situacion)
    {
        // DESCRIPCION: GUARDAR DOCUMENTO ACEPTADO

        string nrocomprobElect = oDocumento.Serie + "-" + oDocumento.Numero;        // NUMERO DE COMPROBANTE ELECTRONICO
        
        try
        {
            
            _sqlConexion.Open();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
            _sqlCommand.Connection = _sqlConexion;
            _sqlCommand.CommandText = "Usp_guardar_facturaelectronicaObservado";
            _sqlCommand.Parameters.AddWithValue("@empresa_id", oDocumento.Idempresa);
            _sqlCommand.Parameters.AddWithValue("@documento_id", oDocumento.Id);
            _sqlCommand.Parameters.AddWithValue("@tipodocumento_id", oDocumento.Idtipodocumento);
            _sqlCommand.Parameters.AddWithValue("@numcompelectronico", nrocomprobElect);
            _sqlCommand.Parameters.AddWithValue("@nombrexml", nombrexml);
            _sqlCommand.Parameters.AddWithValue("@xmlenviado", xmlenviado);
            _sqlCommand.Parameters.AddWithValue("@xmlcdr", cdrxml);
            _sqlCommand.Parameters.AddWithValue("@valorresumen", valorResumen);
            _sqlCommand.Parameters.AddWithValue("@firma", firma);
            _sqlCommand.Parameters.AddWithValue("@mensaje", mensaje);
            _sqlCommand.Parameters.AddWithValue("@ticket", ticket);
            _sqlCommand.Parameters.AddWithValue("@situacion", situacion);
            
            _sqlCommand.ExecuteNonQuery();
            
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }

    public void GuardarDocumentoRechazadoError(EN_Documento oDocumento, string nombrexml, string xmlenviado, string cdrxml, string valorResumen, string firma, string errortext, string estado, string ticket)
    {
        // GUARDAR DOCUMENTO RECHAZADO ERROR

         string nrocomprobElect = oDocumento.Serie + "-" + oDocumento.Numero;   // NUMERO DE COMPROBANTE
        
        try
        {
           
            _sqlConexion.Open();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
            _sqlCommand.Connection = _sqlConexion;
            _sqlCommand.CommandText = "Usp_guardar_facturarechazadaerror";
            _sqlCommand.Parameters.AddWithValue("@empresa_id", oDocumento.Idempresa);
            _sqlCommand.Parameters.AddWithValue("@documento_id", oDocumento.Id);
            _sqlCommand.Parameters.AddWithValue("@tipodocumento_id", oDocumento.Idtipodocumento);
            _sqlCommand.Parameters.AddWithValue("@numcomprobante", nrocomprobElect);
            _sqlCommand.Parameters.AddWithValue("@nombrexml", nombrexml);
            _sqlCommand.Parameters.AddWithValue("@xmlbase64", xmlenviado);
            _sqlCommand.Parameters.AddWithValue("@cdrxml", cdrxml);
            _sqlCommand.Parameters.AddWithValue("@valorresumen", valorResumen);
            _sqlCommand.Parameters.AddWithValue("@firma", firma);
            _sqlCommand.Parameters.AddWithValue("@mensajeerror", Strings.Replace(errortext, "'", "''"));
            _sqlCommand.Parameters.AddWithValue("@estado", estado);
            _sqlCommand.Parameters.AddWithValue("@ticket", ticket);
           
            _sqlCommand.ExecuteNonQuery();
           
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }

    public bool ActualizarSituacion(EN_Documento oDocumento, string situacion)
    {   
            // DESCRIPCION: ACTUALIZAR SITUACION
          
                try
                {
                    _sqlConexion.Open();
                    _sqlCommand.CommandType = CommandType.StoredProcedure;
                    _sqlCommand.Connection = _sqlConexion;
                    _sqlCommand.CommandText = "actualizar_facturasituacion";
                    _sqlCommand.Parameters.AddWithValue("@empresa_id", oDocumento.Idempresa);
                    _sqlCommand.Parameters.AddWithValue("@documento_id", oDocumento.Id);
                    _sqlCommand.Parameters.AddWithValue("@tipodocumento_id", oDocumento.Idtipodocumento);
                    _sqlCommand.Parameters.AddWithValue("@situacion", situacion);
                   
                    _sqlCommand.ExecuteNonQuery();
                   
                    return true;
                }
                catch (Exception ex)
                {
                   
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }


             public List<EN_SituacionBaja> consultarSituacionCpeCB(int id_baja, int empresa_id, int puntoventa_id, int flag)
            {
                // DESCRIPCION: CONSULTA SITUACION DE COMPROBANTE PARA PROCEDER A COMUNICADO DE BAJA
                // string situacion = ""; // CODIGO SUNAT
                SqlCommand cmd ; // COMANDO
                SqlDataReader Rs; // DATA READER

                List<EN_SituacionBaja> listaSituacionBaja = new List<EN_SituacionBaja>();
                EN_SituacionBaja situacionbaja;

                try
                {
             
                 _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_validarAceptado_cpe",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id_baja", id_baja);
                    cmd.Parameters.AddWithValue("@id_empresa",empresa_id);
                    cmd.Parameters.AddWithValue("@id_puntoventa",puntoventa_id);
                    cmd.Parameters.AddWithValue("@flag",flag);

                    Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                        {
                            
                            situacionbaja = new EN_SituacionBaja();
                            situacionbaja.situacion = Conversions.ToString(Rs["situacion"]);
                            situacionbaja.series = Conversions.ToString(Rs["series"]);
                            listaSituacionBaja.Add(situacionbaja);
                        }
                           
                    }

                    Rs.Close();
                    return listaSituacionBaja;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }




        
        }
    
}
