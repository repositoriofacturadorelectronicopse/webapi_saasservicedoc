﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_Resumen.cs
 VERSION : 1.0
 OBJETIVO: Clase de acceso a datos resumen
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.Collections.Generic;
using Microsoft.VisualBasic;
using log4net;
using CEN;
using System.Data.SqlClient;
using System.Data;
using Microsoft.VisualBasic.CompilerServices;

namespace CAD
{
    public class AD_Resumen
    {
       private readonly AD_Cado _datosConexion;         // CLASE CONEXION CADO
        private readonly SqlConnection _sqlConexion;    // CONEXION SQL
        private readonly SqlCommand _sqlCommand;        // COMANDO SQL
        private string TiempoEspera {get;}              

    public AD_Resumen()
    {
        _datosConexion = new AD_Cado();
        _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
        _sqlCommand = new SqlCommand();
         TiempoEspera=EN_ConfigConstantes.Instance.const_TiempoEspera;
    }
   
    public List<EN_DetalleResumen> listarDetalleResumen(EN_Resumen pResumen)
    {
         // DESCRIPCION: LISTAR DETALLE RESUMEN

        List<EN_DetalleResumen> olstDetalleResumen= new List<EN_DetalleResumen>();     // LISTA DE CLASE ENTIDAD DETALLE RESUMEN
        EN_DetalleResumen oDetalleResumen_EN;           // CLASE ENTIDAD DETALLE RESUMEN
        SqlCommand cmd;                                 // COMANDO SQL
        SqlDataReader Rs;                               // DATA READER SQL
        
        try
        {
            
        
            var sql = "select db.*, t.descripcion from detalleresumen db inner join tipodocumento t on db.tipodocumento_id = t.id where 1=1 ";
            if ((pResumen.Id.ToString() != "0"))
                sql += " and db.resumen_id = @param1";

            cmd = new SqlCommand(sql, _sqlConexion);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(new SqlParameter("param1", pResumen.Id.ToString()));

            cmd.CommandTimeout = Convert.ToInt32(TiempoEspera);
            _sqlConexion.Open();
            Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (Rs.HasRows)
            {
                while (Rs.Read())
                {
                    oDetalleResumen_EN = new EN_DetalleResumen();
                    {
                        var withBlock = oDetalleResumen_EN;
                        withBlock.IdResumen = (int)Rs["resumen_id"];
                        withBlock.Item = (int)Rs["item"];
                        withBlock.Tipodocumentoid = (string)Rs["tipodocumento_id"];
                        withBlock.DescTipodocumento = (string)Rs["descripcion"];
                        withBlock.Serie = (string)Rs["serie"];
                        withBlock.Correlativo = (string)Rs["correlativo"];
                        withBlock.DocCliente = (string)Rs["doccliente"];
                        withBlock.NroDocCliente = (string)Rs["nrodoccliente"];
                        withBlock.Condicion = (int)Rs["condicion"];
                        withBlock.Moneda = (string)Rs["moneda"];
                        withBlock.Opegravadas = (decimal)Interaction.IIf(Information.IsDBNull(Rs["opegravadas"]), 0, Rs["opegravadas"]);
                        withBlock.Opeexoneradas = (decimal)Interaction.IIf(Information.IsDBNull(Rs["opeexoneradas"]), 0, Rs["opeexoneradas"]);
                        withBlock.Opeinafectas = (decimal)Interaction.IIf(Information.IsDBNull(Rs["opeinafectas"]), 0, Rs["opeinafectas"]);
                        withBlock.Opegratuitas = (decimal)Interaction.IIf(Information.IsDBNull(Rs["opegratuitas"]), 0, Rs["opegratuitas"]);
                        withBlock.Otroscargos = (decimal)Interaction.IIf(Information.IsDBNull(Rs["otroscargos"]), 0, Rs["otroscargos"]);
                        withBlock.TotalISC = (decimal)Interaction.IIf(Information.IsDBNull(Rs["totalISC"]), 0, Rs["totalISC"]);
                        withBlock.TotalIGV = (decimal)Interaction.IIf(Information.IsDBNull(Rs["totalIGV"]), 0, Rs["totalIGV"]);
                        withBlock.Otrostributos = (decimal)Interaction.IIf(Information.IsDBNull(Rs["otrostributos"]), 0, Rs["otrostributos"]);
                        withBlock.Importeventa = (decimal)Rs["importeventa"];
                        withBlock.Tipodocref = (string)Interaction.IIf(Information.IsDBNull(Rs["tipodocref"]), "", Rs["tipodocref"]);
                        withBlock.Nrodocref = (string)Interaction.IIf(Information.IsDBNull(Rs["nrodocref"]), "", Rs["nrodocref"]);
                        withBlock.Regimenpercep = (string)Interaction.IIf(Information.IsDBNull(Rs["regimenpercep"]), "", Rs["regimenpercep"]);
                        withBlock.Porcentpercep = (decimal)Interaction.IIf(Information.IsDBNull(Rs["porcentpercep"]), 0, Rs["porcentpercep"]);
                        withBlock.Importepercep = (decimal)Interaction.IIf(Information.IsDBNull(Rs["importepercep"]), 0, Rs["importepercep"]);
                        withBlock.Importefinal = (decimal)Rs["importefinal"];
                    }
                    olstDetalleResumen.Add(oDetalleResumen_EN);
                }
            }
            Rs.Close();
            
            return olstDetalleResumen;
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }

    
    public void GuardarResumenAceptada(EN_Resumen oResumen, string nombrexml, string xmlbase64, string cdrxml, string valorResumen, string firma, string mensaje, string ticket)
    {
        // DESCRIPCION: GUARDAR RESUMEN ACEPTADA
       
        string identresumen;            // ID RESUMEN
        identresumen = oResumen.Tiporesumen.Trim() + "-"  + 
          Strings.Format(Conversions.ToDate(oResumen.FechaGeneraResumen), "yyyyMMdd") +
         "-" + oResumen.Correlativo;
       
        try
        {
            _sqlConexion.Open();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
            _sqlCommand.Connection = _sqlConexion;
            _sqlCommand.CommandText = "Usp_guardar_resumenelectronico";
            _sqlCommand.Parameters.AddWithValue("@resumen_id", oResumen.Id);
            _sqlCommand.Parameters.AddWithValue("@empresa_id", oResumen.Empresa_id);
            _sqlCommand.Parameters.AddWithValue("@identifica_resumen", identresumen);
            _sqlCommand.Parameters.AddWithValue("@nombrexml", nombrexml);
            _sqlCommand.Parameters.AddWithValue("@xmlbase64", xmlbase64);
            _sqlCommand.Parameters.AddWithValue("@xmlcdr", cdrxml);
            _sqlCommand.Parameters.AddWithValue("@valorresumen", valorResumen);
            _sqlCommand.Parameters.AddWithValue("@firma", firma);
            _sqlCommand.Parameters.AddWithValue("@mensaje", mensaje);
            _sqlCommand.Parameters.AddWithValue("@ticket", ticket);

  
            _sqlCommand.ExecuteNonQuery();
     
        }
        catch (Exception ex)
        {
           
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public void GuardarResumenRechazadaError(EN_Resumen oResumen, string nombrexml, string xmlbase64, string cdrxml, string valorResumen, string firma, string errortext, string estado, string ticket)
    {
        // DESCRIPCION: GUARDAR RESUMEN RECHAZADA ERROR
       
        string identresumen;            // ID RESUMEN
        identresumen = oResumen.Tiporesumen.Trim() + "-" +
        Strings.Format(Conversions.ToDate(oResumen.FechaGeneraResumen), "yyyyMMdd")
         + "-" + oResumen.Correlativo;
      
        try
        {
            _sqlConexion.Open();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
            _sqlCommand.Connection = _sqlConexion;
            _sqlCommand.CommandText = "Usp_guardar_resumenrechazadoerror";
            _sqlCommand.Parameters.AddWithValue("@resumen_id", oResumen.Id);
            _sqlCommand.Parameters.AddWithValue("@empresa_id", oResumen.Empresa_id);
            _sqlCommand.Parameters.AddWithValue("@identifica_resumen", identresumen);
            _sqlCommand.Parameters.AddWithValue("@nombrexml", nombrexml);
            _sqlCommand.Parameters.AddWithValue("@xmlbase64", xmlbase64);
            _sqlCommand.Parameters.AddWithValue("@xmlcdr", cdrxml);
            _sqlCommand.Parameters.AddWithValue("@valorresumen", valorResumen);
            _sqlCommand.Parameters.AddWithValue("@firma", firma);
            _sqlCommand.Parameters.AddWithValue("@mensajeerror", Strings.Replace(errortext, "'", "''"));
            _sqlCommand.Parameters.AddWithValue("@estado", estado);
            _sqlCommand.Parameters.AddWithValue("@ticket", ticket);
           
            _sqlCommand.ExecuteNonQuery();
       
        }
        catch (Exception ex)
        {
          
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    
    
     
    }
}