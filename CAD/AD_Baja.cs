/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_Baja.cs
 VERSION : 1.0
 OBJETIVO: Clase de acceso a baja
 FECHA   : 20/07/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using log4net;
using Microsoft.VisualBasic; // Install-Package Microsoft.VisualBasic
using Microsoft.VisualBasic.CompilerServices; // Install-Package Microsoft.VisualBasic
using CEN;
using System.Data;

namespace CAD
{
    public class AD_Baja
    {
        private readonly AD_Cado _datosConexion;         // CLASE DE CONEXION CADO
        private readonly SqlConnection _sqlConexion;     // CONEXION SQL
        private readonly SqlCommand _sqlCommand;         // COMANDO SQL

        public AD_Baja()
        {
             // DESCRIPCION: CONSTRUCTOR DE CLASE

            _datosConexion = new AD_Cado();
            _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
            _sqlCommand = new SqlCommand();
         
        }

         public List<EN_DetalleBaja> listarDetalleBaja(EN_Baja pBaja)
        {
            //DESCRIPCION: FUNCION LISTAR DETALLE BAJA

            SqlCommand cmd; // comando para certificado
            SqlDataReader Rs;   //Data Reader para certificado
            var olstDetalleBaja = new List<EN_DetalleBaja>();
            EN_DetalleBaja oDetalleBaja_EN;

            try
            {
                _sqlConexion.Open();
                cmd= new SqlCommand("Usp_listar_detalleBaja",_sqlConexion);
                cmd.CommandType= CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id_baja",  pBaja.Id);
               
              
                Rs = cmd.ExecuteReader();

               
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        oDetalleBaja_EN = new EN_DetalleBaja();
                        oDetalleBaja_EN.IdBaja = (int)Rs["baja_id"];
                        oDetalleBaja_EN.Item = (int)Rs["item"];
                        oDetalleBaja_EN.Tipodocumentoid = (string)Rs["tipodocumento_id"];
                        oDetalleBaja_EN.DescTipodocumento = (string)Rs["descripcion"];
                        oDetalleBaja_EN.Serie = (string)Rs["serie"];
                        oDetalleBaja_EN.Numero = (string)Rs["numero"];
                        oDetalleBaja_EN.MotivoBaja =(string) Rs["motivobaja"];
                        olstDetalleBaja.Add(oDetalleBaja_EN);
                    }
                }

                Rs.Close();
                return olstDetalleBaja;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }

        public void GuardarBajaAceptada(EN_Baja oBaja, string nombrexml, string xmlbase64, string cdrxml, string valorResumen, string firma, string mensaje, string ticket)
        {
             // DESCRIPCION: FUNCION PARA GUARDAR COMUNICACION DE BAJA ACEPTADA
            
            string identcomunicacion = "";      // IDENTIFICADOR DE COMUNICACION DE BAJA
            
            try
            {
                identcomunicacion = oBaja.Tiporesumen.Trim() + "-" + Strings.Format(Conversions.ToDate(oBaja.FechaComunicacion),  EN_Constante.g_const_formfecha_yyyyMMdd) + "-" + oBaja.Correlativo;

                _sqlConexion.Open();
                _sqlCommand.CommandType = CommandType.StoredProcedure;
                _sqlCommand.Connection = _sqlConexion;
                _sqlCommand.CommandText = "Usp_guardar_bajaelectronica";
                _sqlCommand.Parameters.AddWithValue("@baja_id", oBaja.Id);
                _sqlCommand.Parameters.AddWithValue("@empresa_id", oBaja.Empresa_id);
                _sqlCommand.Parameters.AddWithValue("@identifica_comunicacion", identcomunicacion);
                _sqlCommand.Parameters.AddWithValue("@nombrexml", nombrexml);
                _sqlCommand.Parameters.AddWithValue("@xmlbase64", xmlbase64);
                _sqlCommand.Parameters.AddWithValue("@xmlcdr", cdrxml);
                _sqlCommand.Parameters.AddWithValue("@valorresumen", valorResumen);
                _sqlCommand.Parameters.AddWithValue("@firma", firma);
                _sqlCommand.Parameters.AddWithValue("@mensaje", mensaje);
                _sqlCommand.Parameters.AddWithValue("@ticket", ticket);
               
                _sqlCommand.ExecuteNonQuery();
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }

        public void GuardarBajaRechazadaError(EN_Baja oBaja, string nombrexml, string xmlbase64, string cdrxml, string valorResumen, string firma, string errortext, string estado, string ticket)
        {
            // DESCRIPCION: FUNCION PARA GUARDAR COMUNICACION DE BAJA RECHAZADA O CON ERROR
                      
            string identcomunicacion = "";      // IDENTIFICADOR DE COMUNICACION DE BAJA
             
           
            try
            {
                identcomunicacion=oBaja.Tiporesumen.Trim() + "-" + Strings.Format(Conversions.ToDate(oBaja.FechaComunicacion), EN_Constante.g_const_formfecha_yyyyMMdd) + "-" + oBaja.Correlativo;

                _sqlConexion.Open();
                _sqlCommand.CommandType = CommandType.StoredProcedure;
                _sqlCommand.Connection = _sqlConexion;
                _sqlCommand.CommandText = "Usp_guardar_bajarechazadaerror";
                _sqlCommand.Parameters.AddWithValue("@baja_id", oBaja.Id);
                _sqlCommand.Parameters.AddWithValue("@empresa_id", oBaja.Empresa_id);
                _sqlCommand.Parameters.AddWithValue("@identifica_comunicacion", identcomunicacion);
                _sqlCommand.Parameters.AddWithValue("@nombrexml", nombrexml);
                _sqlCommand.Parameters.AddWithValue("@xmlbase64", xmlbase64);
                _sqlCommand.Parameters.AddWithValue("@xmlcdr", cdrxml);
                _sqlCommand.Parameters.AddWithValue("@valorresumen", valorResumen);
                _sqlCommand.Parameters.AddWithValue("@firma", firma);
                _sqlCommand.Parameters.AddWithValue("@mensajeerror", Strings.Replace(errortext, "'", "''"));
                _sqlCommand.Parameters.AddWithValue("@estado", estado);
                _sqlCommand.Parameters.AddWithValue("@ticket", ticket);
                
                _sqlCommand.ExecuteNonQuery();
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }

        // public void ActualizarEnvioMail(EN_Baja oBaja, string estado)
        // {
        //     // Llenamos los valores para el LOG
        //     var objLog = new clsLogEN();
        //     objLog.ClaseDatos = "AD_Baja";
        //     objLog.MetodoFuncionClase = "ActualizarEnvioMail";
        //     objLog.CadenaAleatoria = "";
        //     objLog.UsuarioSession = "";
        //     var list = new ArrayList();
        //     list.Add("IdResumen: " + oBaja.Id);
        //     list.Add("IdEmpresa: " + oBaja.Empresa_id);
        //     list.Add("Tiporesumen: " + oBaja.Tiporesumen);
        //     list.Add("EnvioMail: " + estado);
        //     try
        //     {
        //         _sqlConexion.Open();
        //         _sqlCommand.CommandType = CommandType.StoredProcedure;
        //         _sqlCommand.Connection = _sqlConexion;
        //         _sqlCommand.CommandText = "actualizar_bajaenviomail";
        //         _sqlCommand.Parameters.AddWithValue("@baja_id", oBaja.Id);
        //         _sqlCommand.Parameters.AddWithValue("@empresa_id", oBaja.Empresa_id);
        //         _sqlCommand.Parameters.AddWithValue("@estado", estado);
        //         // Guardamos Log
        //         clsLogAD.GuardarInicioLogOperacion(objLog);
        //         clsLogAD.GuardarLogOperacion(objLog, list);
        //         _sqlCommand.ExecuteNonQuery();
        //         // Guardamos final operacion en el log
        //         objLog.MensajeAuditoriaOk = "Actualizar EnvioMail Correcto";
        //         clsLogAD.GuardarFinLogOperacion(objLog, true);
        //     }
        //     catch (Exception ex)
        //     {
        //         // Guardamos error en el log
        //         objLog.MensajeAuditoriaError = "Error al Actualizar EnvioMail";
        //         clsLogAD.GuardarErrorLogOperacion(objLog, ex);
        //         throw ex;
        //     }
        //     finally
        //     {
        //         _sqlConexion.Close();
        //     }
        // }

        // public List<EN_DocElectronico> consultarDocumentoElectronico(EN_DocElectronico pDocumento)
        // {
        //     // Llenamos los valores para el LOG
        //     var objLog = new clsLogEN();
        //     objLog.ClaseDatos = "AD_Baja";
        //     objLog.MetodoFuncionClase = "consultarDocumentoElectronico";
        //     objLog.CadenaAleatoria = pDocumento.CadenaAleatoria;
        //     objLog.UsuarioSession = pDocumento.UsuarioSession;
        //     var list = new ArrayList();
        //     list.Add("RucEmisor: " + pDocumento.RucEmisor);
        //     list.Add("IdTipoDocumento: " + pDocumento.Idtipodocumento);
        //     list.Add("Numero: " + pDocumento.Numero);
        //     list.Add("FechaDoc: " + pDocumento.FechaEmision);
        //     // Fin llenado
        //     try
        //     {
        //         // Guardamos Log
        //         clsLogAD.GuardarInicioLogOperacion(objLog);
        //         clsLogAD.GuardarLogOperacion(objLog, list);
        //         var olstDocumento = new List<EN_DocElectronico>();
        //         EN_DocElectronico oDocumento_EN;
        //         string sql;
        //         sql = "SELECT d.empresa_id,e.nrodocumento,d.id,d.tiporesumen,d.correlativo,d.fecgenera_comunicacion, " + "d.identifica_comunicacion,d.nombrexml,d.valorresumen,d.valorfirma,d.xmlgenerado FROM baja d " + "inner join empresa e on e.id=d.empresa_id " + "WHERE e.nrodocumento = @param1 AND d.tiporesumen = @param2 " + "AND d.correlativo = @param3 AND d.fecgenera_comunicacion = @param4";
        //         var cmd = new SqlCommand(sql, _sqlConexion);
        //         cmd.CommandType = CommandType.Text;
        //         cmd.Parameters.Add(new SqlParameter("param1", pDocumento.RucEmisor));
        //         cmd.Parameters.Add(new SqlParameter("param2", pDocumento.Idtipodocumento));
        //         cmd.Parameters.Add(new SqlParameter("param3", pDocumento.Numero));
        //         cmd.Parameters.Add(new SqlParameter("param4", pDocumento.FechaEmision));
        //         cmd.CommandTimeout = ConfigurationManager.AppSettings("TiempoEspera").ToString();
        //         _sqlConexion.Open();
        //         var Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        //         if (Rs.HasRows)
        //         {
        //             while (Rs.Read())
        //             {
        //                 oDocumento_EN = new EN_DocElectronico();
        //                 oDocumento_EN.Idempresa = Rs["empresa_id"];
        //                 oDocumento_EN.RucEmisor = Rs["nrodocumento"];
        //                 oDocumento_EN.IdDocumento = Rs["id"];
        //                 oDocumento_EN.Idtipodocumento = Rs["tiporesumen"];
        //                 oDocumento_EN.Numero = Rs["correlativo"];
        //                 oDocumento_EN.FechaEmision = Rs["fecgenera_comunicacion"];
        //                 oDocumento_EN.Numcompelectronico = Interaction.IIf(Information.IsDBNull(Rs["identifica_comunicacion"]), "", Rs["identifica_comunicacion"]);
        //                 oDocumento_EN.NombreXML = Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
        //                 oDocumento_EN.ValorResumen = Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
        //                 oDocumento_EN.Firma = Interaction.IIf(Information.IsDBNull(Rs["valorfirma"]), "", Rs["valorfirma"]);
        //                 oDocumento_EN.xmlGenerado = Interaction.IIf(Information.IsDBNull(Rs["xmlgenerado"]), "", Rs["xmlgenerado"]);
        //                 olstDocumento.Add(oDocumento_EN);
        //             }
        //         }

        //         Rs.Close();
        //         // Guardamos final operacion en el log
        //         objLog.MensajeAuditoriaOk = "consultado correctamente";
        //         clsLogAD.GuardarFinLogOperacion(objLog, true);
        //         return olstDocumento;
        //     }
        //     catch (Exception ex)
        //     {
        //         // Guardamos error en el log
        //         clsLogAD.GuardarErrorLogOperacion(objLog, ex);
        //         throw ex;
        //     }
        //     finally
        //     {
        //         _sqlConexion.Close();
        //     }
        // }

        // public List<EN_Baja> listarAceptadoBaja(EN_Baja pDocumento)
        // {
        //     // Llenamos los valores para el LOG
        //     var objLog = new clsLogEN();
        //     objLog.ClaseDatos = "AD_Baja";
        //     objLog.MetodoFuncionClase = "listarAceptadoBaja";
        //     objLog.CadenaAleatoria = pDocumento.CadenaAleatoria;
        //     objLog.UsuarioSession = pDocumento.UsuarioSession;
        //     var list = new ArrayList();
        //     list.Add("IdBaja: " + pDocumento.Id);
        //     list.Add("IdEmpresa: " + pDocumento.Empresa_id);
        //     // Fin llenado
        //     try
        //     {
        //         var olstDocumento = new List<EN_Baja>();
        //         EN_Baja oDocumento_EN;

        //         // Guardamos Log
        //         clsLogAD.GuardarInicioLogOperacion(objLog);
        //         clsLogAD.GuardarLogOperacion(objLog, list);
        //         string sql;
        //         sql = "select baja_id,empresa_id,identifica_comunicacion,nombrexml,xmlbase64,cdrxml,valorresumen,firma,mensaje,fechaenvio,ticket from bajaelectronica d where 1=1  ";
        //         if (pDocumento.Id.ToString != "")
        //         {
        //             sql += " and d.baja_id = @param1";
        //         }

        //         if (pDocumento.Empresa_id.ToString != "0")
        //         {
        //             sql += " and d.empresa_id = @param2";
        //         }

        //         sql += " order by d.fechaenvio desc";
        //         var cmd = new SqlCommand(sql, _sqlConexion);
        //         cmd.CommandType = CommandType.Text;
        //         cmd.Parameters.Add(new SqlParameter("param1", pDocumento.Id.ToString()));
        //         cmd.Parameters.Add(new SqlParameter("param2", pDocumento.Empresa_id.ToString()));
        //         cmd.CommandTimeout = ConfigurationManager.AppSettings("TiempoEspera").ToString();
        //         _sqlConexion.Open();
        //         var Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        //         if (Rs.HasRows)
        //         {
        //             while (Rs.Read())
        //             {
        //                 oDocumento_EN = new EN_Baja();
        //                 oDocumento_EN.Id = Rs["baja_id"];
        //                 oDocumento_EN.Empresa_id = Rs["empresa_id"];
        //                 oDocumento_EN.Identificacomunicacion = Interaction.IIf(Information.IsDBNull(Rs["identifica_comunicacion"]), "", Rs["identifica_comunicacion"]);
        //                 oDocumento_EN.nombreXML = Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
        //                 oDocumento_EN.XMLEnviado = Interaction.IIf(Information.IsDBNull(Rs["xmlbase64"]), "", Rs["xmlbase64"]);
        //                 oDocumento_EN.CDRXML = Interaction.IIf(Information.IsDBNull(Rs["cdrxml"]), "", Rs["cdrxml"]);
        //                 oDocumento_EN.vResumen = Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
        //                 oDocumento_EN.vFirma = Interaction.IIf(Information.IsDBNull(Rs["firma"]), "", Rs["firma"]);
        //                 oDocumento_EN.MensajeCDR = Regex.Replace(Conversions.ToString(Rs["mensaje"]), "[^a-zA-Z0-9_.-]+ ", "", RegexOptions.Compiled); // Rs("mensajeerror")
        //                 oDocumento_EN.FechaEnvio = Rs["fechaenvio"];
        //                 oDocumento_EN.ticket = Interaction.IIf(Information.IsDBNull(Rs["ticket"]), "", Rs["ticket"]);
        //                 olstDocumento.Add(oDocumento_EN);
        //             }
        //         }

        //         Rs.Close();
        //         // Guardamos final operacion en el log
        //         objLog.MensajeAuditoriaOk = "listado correctamente";
        //         clsLogAD.GuardarFinLogOperacion(objLog, true);
        //         return olstDocumento;
        //     }
        //     catch (Exception ex)
        //     {
        //         // Guardamos error en el log
        //         clsLogAD.GuardarErrorLogOperacion(objLog, ex);
        //         throw ex;
        //     }
        //     finally
        //     {
        //         _sqlConexion.Close();
        //     }
        // }

        // public List<EN_Baja> listarErrorRechazoBaja(EN_Baja pDocumento)
        // {
        //     // Llenamos los valores para el LOG
        //     var objLog = new clsLogEN();
        //     objLog.ClaseDatos = "AD_Baja";
        //     objLog.MetodoFuncionClase = "listarErrorRechazoBaja";
        //     objLog.CadenaAleatoria = pDocumento.CadenaAleatoria;
        //     objLog.UsuarioSession = pDocumento.UsuarioSession;
        //     var list = new ArrayList();
        //     list.Add("IdBaja: " + pDocumento.Id);
        //     list.Add("IdEmpresa: " + pDocumento.Empresa_id);
        //     // Fin llenado
        //     try
        //     {
        //         var olstDocumento = new List<EN_Baja>();
        //         EN_Baja oDocumento_EN;

        //         // Guardamos Log
        //         clsLogAD.GuardarInicioLogOperacion(objLog);
        //         clsLogAD.GuardarLogOperacion(objLog, list);
        //         string sql;
        //         sql = "select baja_id,empresa_id,identifica_comunicacion,nombrexml,xmlbase64,cdrxml,valorresumen,firma,mensajeerror,fecharechazo,ticket from bajarechazoerror d where 1=1  ";
        //         if (pDocumento.Id.ToString != "")
        //         {
        //             sql += " and d.baja_id = @param1";
        //         }

        //         if (pDocumento.Empresa_id.ToString != "0")
        //         {
        //             sql += " and d.empresa_id = @param2";
        //         }

        //         sql += " order by d.fecharechazo desc";
        //         var cmd = new SqlCommand(sql, _sqlConexion);
        //         cmd.CommandType = CommandType.Text;
        //         cmd.Parameters.Add(new SqlParameter("param1", pDocumento.Id.ToString()));
        //         cmd.Parameters.Add(new SqlParameter("param2", pDocumento.Empresa_id.ToString()));
        //         cmd.CommandTimeout = ConfigurationManager.AppSettings("TiempoEspera").ToString();
        //         _sqlConexion.Open();
        //         var Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        //         if (Rs.HasRows)
        //         {
        //             while (Rs.Read())
        //             {
        //                 oDocumento_EN = new EN_Baja();
        //                 oDocumento_EN.Id = Rs["baja_id"];
        //                 oDocumento_EN.Empresa_id = Rs["empresa_id"];
        //                 oDocumento_EN.Identificacomunicacion = Interaction.IIf(Information.IsDBNull(Rs["identifica_comunicacion"]), "", Rs["identifica_comunicacion"]);
        //                 oDocumento_EN.nombreXML = Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
        //                 oDocumento_EN.XMLEnviado = Interaction.IIf(Information.IsDBNull(Rs["xmlbase64"]), "", Rs["xmlbase64"]);
        //                 oDocumento_EN.CDRXML = Interaction.IIf(Information.IsDBNull(Rs["cdrxml"]), "", Rs["cdrxml"]);
        //                 oDocumento_EN.vResumen = Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
        //                 oDocumento_EN.vFirma = Interaction.IIf(Information.IsDBNull(Rs["firma"]), "", Rs["firma"]);
        //                 oDocumento_EN.MensajeError = Regex.Replace(Conversions.ToString(Rs["mensajeerror"]), "[^a-zA-Z0-9_.-]+ ", "", RegexOptions.Compiled); // Rs("mensajeerror")
        //                 oDocumento_EN.FechaError = Rs["fecharechazo"];
        //                 oDocumento_EN.ticket = Interaction.IIf(Information.IsDBNull(Rs["ticket"]), "", Rs["ticket"]);
        //                 olstDocumento.Add(oDocumento_EN);
        //             }
        //         }

        //         Rs.Close();
        //         // Guardamos final operacion en el log
        //         objLog.MensajeAuditoriaOk = "listado correctamente";
        //         clsLogAD.GuardarFinLogOperacion(objLog, true);
        //         return olstDocumento;
        //     }
        //     catch (Exception ex)
        //     {
        //         // Guardamos error en el log
        //         clsLogAD.GuardarErrorLogOperacion(objLog, ex);
        //         throw ex;
        //     }
        //     finally
        //     {
        //         _sqlConexion.Close();
        //     }
        // }

        // public List<EN_Baja> listarErrorRechazoBajaConTicket(EN_Baja pDocumento)
        // {
        //     // Llenamos los valores para el LOG
        //     var objLog = new clsLogEN();
        //     objLog.ClaseDatos = "AD_Baja";
        //     objLog.MetodoFuncionClase = "listarErrorRechazoBajaConTicket";
        //     objLog.CadenaAleatoria = pDocumento.CadenaAleatoria;
        //     objLog.UsuarioSession = pDocumento.UsuarioSession;
        //     var list = new ArrayList();
        //     list.Add("IdBaja: " + pDocumento.Id);
        //     list.Add("IdEmpresa: " + pDocumento.Empresa_id);
        //     // Fin llenado
        //     try
        //     {
        //         var olstDocumento = new List<EN_Baja>();
        //         EN_Baja oDocumento_EN;

        //         // Guardamos Log
        //         clsLogAD.GuardarInicioLogOperacion(objLog);
        //         clsLogAD.GuardarLogOperacion(objLog, list);
        //         string sql;
        //         sql = "select baja_id,empresa_id,identifica_comunicacion,nombrexml,xmlbase64,cdrxml,valorresumen,firma,mensajeerror,fecharechazo,ticket from bajarechazoerror d where 1=1  ";
        //         if (pDocumento.Id.ToString != "")
        //         {
        //             sql += " and d.baja_id = @param1";
        //         }

        //         if (pDocumento.Empresa_id.ToString != "0")
        //         {
        //             sql += " and d.empresa_id = @param2";
        //         }

        //         sql += " and d.ticket <> '' order by d.fecharechazo desc";
        //         var cmd = new SqlCommand(sql, _sqlConexion);
        //         cmd.CommandType = CommandType.Text;
        //         cmd.Parameters.Add(new SqlParameter("param1", pDocumento.Id.ToString()));
        //         cmd.Parameters.Add(new SqlParameter("param2", pDocumento.Empresa_id.ToString()));
        //         cmd.CommandTimeout = ConfigurationManager.AppSettings("TiempoEspera").ToString();
        //         _sqlConexion.Open();
        //         var Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        //         if (Rs.HasRows)
        //         {
        //             while (Rs.Read())
        //             {
        //                 oDocumento_EN = new EN_Baja();
        //                 oDocumento_EN.Id = Rs["baja_id"];
        //                 oDocumento_EN.Empresa_id = Rs["empresa_id"];
        //                 oDocumento_EN.Identificacomunicacion = Interaction.IIf(Information.IsDBNull(Rs["identifica_comunicacion"]), "", Rs["identifica_comunicacion"]);
        //                 oDocumento_EN.nombreXML = Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
        //                 oDocumento_EN.XMLEnviado = Interaction.IIf(Information.IsDBNull(Rs["xmlbase64"]), "", Rs["xmlbase64"]);
        //                 oDocumento_EN.CDRXML = Interaction.IIf(Information.IsDBNull(Rs["cdrxml"]), "", Rs["cdrxml"]);
        //                 oDocumento_EN.vResumen = Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
        //                 oDocumento_EN.vFirma = Interaction.IIf(Information.IsDBNull(Rs["firma"]), "", Rs["firma"]);
        //                 oDocumento_EN.MensajeError = Regex.Replace(Conversions.ToString(Rs["mensajeerror"]), "[^a-zA-Z0-9_.-]+ ", "", RegexOptions.Compiled); // Rs("mensajeerror")
        //                 oDocumento_EN.FechaError = Rs["fecharechazo"];
        //                 oDocumento_EN.ticket = Interaction.IIf(Information.IsDBNull(Rs["ticket"]), "", Rs["ticket"]);
        //                 olstDocumento.Add(oDocumento_EN);
        //             }
        //         }

        //         Rs.Close();
        //         // Guardamos final operacion en el log
        //         objLog.MensajeAuditoriaOk = "listado correctamente";
        //         clsLogAD.GuardarFinLogOperacion(objLog, true);
        //         return olstDocumento;
        //     }
        //     catch (Exception ex)
        //     {
        //         // Guardamos error en el log
        //         clsLogAD.GuardarErrorLogOperacion(objLog, ex);
        //         throw ex;
        //     }
        //     finally
        //     {
        //         _sqlConexion.Close();
        //     }
        // }

        // public List<EN_Baja> reporteConsolidado(EN_Baja pResumen)
        // {
        //     // Llenamos los valores para el LOG
        //     var objLog = new clsLogEN();
        //     objLog.ClaseDatos = "AD_Baja";
        //     objLog.MetodoFuncionClase = "reporteConsolidado";
        //     objLog.CadenaAleatoria = pResumen.CadenaAleatoria;
        //     objLog.UsuarioSession = pResumen.UsuarioSession;
        //     var list = new ArrayList();
        //     list.Add("IdEmpresa: " + pResumen.Empresa_id);
        //     list.Add("IdTipoDocumento: " + pResumen.Tiporesumen);
        //     list.Add("Situacion: " + pResumen.Situacion);
        //     // Fin llenado
        //     try
        //     {
        //         var olstDocumento = new List<EN_Baja>();
        //         EN_Baja oDocumento_EN;
        //         // Guardamos Log
        //         clsLogAD.GuardarInicioLogOperacion(objLog);
        //         clsLogAD.GuardarLogOperacion(objLog, list);
        //         string sql;
        //         sql = "select t.descripcion, d.situacion, count(*) cantidad from baja d " + "inner join tipodocumento t on d.tiporesumen=t.id " + "where d.estado='N' ";
        //         if (pResumen.Empresa_id.ToString != "0")
        //         {
        //             sql += " and d.empresa_id = @param1";
        //         }

        //         if (pResumen.Tiporesumen.ToString != "")
        //         {
        //             sql += " and d.tiporesumen = @param2";
        //         }

        //         if (pResumen.Situacion.ToString != "")
        //         {
        //             sql += " and d.situacion = @param3";
        //         }

        //         if (pResumen.FechaIni != "" & pResumen.FechaFin != "")
        //         {
        //             sql += " and d.fecgenera_comunicacion between '" + Strings.Format(Conversions.ToDate(pResumen.FechaIni), "yyyy-MM-dd") + "' and '" + Strings.Format(Conversions.ToDate(pResumen.FechaFin), "yyyy-MM-dd") + "'";
        //         }

        //         sql += " group by t.descripcion, d.situacion order by t.descripcion, d.situacion";
        //         var cmd = new SqlCommand(sql, _sqlConexion);
        //         cmd.CommandType = CommandType.Text;
        //         cmd.Parameters.Add(new SqlParameter("param1", pResumen.Empresa_id.ToString()));
        //         cmd.Parameters.Add(new SqlParameter("param2", pResumen.Tiporesumen.ToString()));
        //         cmd.Parameters.Add(new SqlParameter("param3", pResumen.Situacion.ToString()));
        //         cmd.CommandTimeout = ConfigurationManager.AppSettings("TiempoEspera").ToString();
        //         _sqlConexion.Open();
        //         var Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        //         if (Rs.HasRows)
        //         {
        //             while (Rs.Read())
        //             {
        //                 oDocumento_EN = new EN_Baja();
        //                 oDocumento_EN.Tiporesumen = Rs["descripcion"];
        //                 oDocumento_EN.Situacion = Rs["situacion"];
        //                 oDocumento_EN.Cantidad = Rs["cantidad"];
        //                 olstDocumento.Add(oDocumento_EN);
        //             }
        //         }

        //         Rs.Close();
        //         // Guardamos final operacion en el log
        //         objLog.MensajeAuditoriaOk = "listado correctamente";
        //         clsLogAD.GuardarFinLogOperacion(objLog, true);
        //         return olstDocumento;
        //     }
        //     catch (Exception ex)
        //     {
        //         // Guardamos error en el log
        //         clsLogAD.GuardarErrorLogOperacion(objLog, ex);
        //         throw ex;
        //     }
        //     finally
        //     {
        //         _sqlConexion.Close();
        //     }
        // }

        // public List<EN_Baja> listarBajaAsociada(EN_Documento pDocumento)
        // {
        //     // Llenamos los valores para el LOG
        //     var objLog = new clsLogEN();
        //     objLog.ClaseDatos = "AD_Baja";
        //     objLog.MetodoFuncionClase = "listarBajaAsociada";
        //     objLog.CadenaAleatoria = pDocumento.CadenaAleatoria;
        //     objLog.UsuarioSession = pDocumento.UsuarioSession;
        //     var list = new ArrayList();
        //     list.Add("IdEmpresa: " + pDocumento.Idempresa);
        //     list.Add("IdTipoDocumento: " + pDocumento.Idtipodocumento);
        //     list.Add("Serie: " + pDocumento.Serie);
        //     list.Add("Numero: " + pDocumento.Numero);
        //     list.Add("Estado: " + pDocumento.Estado);
        //     list.Add("Situacion: " + pDocumento.Situacion);
        //     // Fin llenado
        //     try
        //     {
        //         var olstBaja = new List<EN_Baja>();
        //         EN_Baja oBaja_EN;
        //         // Guardamos Log
        //         clsLogAD.GuardarInicioLogOperacion(objLog);
        //         clsLogAD.GuardarLogOperacion(objLog, list);
        //         string sql;
        //         sql = "select id, empresa_id, tiporesumen, correlativo, fecgenera_documento, fecgenera_comunicacion, identifica_comunicacion, estado, situacion, enviado_email, nombrexml, valorresumen, valorfirma  from baja b inner join detallebaja d on b.id=d.baja_id WHERE 1=1 ";
        //         if (pDocumento.Idempresa.ToString != "0")
        //         {
        //             sql += " and b.empresa_id = @param1";
        //         }

        //         if (pDocumento.Idtipodocumento.ToString != "")
        //         {
        //             sql += " and d.tipodocumento_id = @param2";
        //         }

        //         if (pDocumento.Serie.ToString != "")
        //         {
        //             sql += " and d.serie = @param3";
        //         }

        //         if (pDocumento.Numero.ToString != "")
        //         {
        //             sql += " and convert(int,d.numero) = @param4";
        //         }

        //         if (pDocumento.Estado.ToString != "")
        //         {
        //             sql += " and b.estado = @param5";
        //         }

        //         if (pDocumento.Situacion.ToString != "")
        //         {
        //             sql += " and b.situacion = @param6";
        //         }

        //         var cmd = new SqlCommand(sql, _sqlConexion);
        //         cmd.CommandType = CommandType.Text;
        //         cmd.Parameters.Add(new SqlParameter("param1", pDocumento.Idempresa.ToString()));
        //         cmd.Parameters.Add(new SqlParameter("param2", pDocumento.Idtipodocumento.ToString()));
        //         cmd.Parameters.Add(new SqlParameter("param3", pDocumento.Serie.ToString()));
        //         cmd.Parameters.Add(new SqlParameter("param4", pDocumento.Numero));
        //         cmd.Parameters.Add(new SqlParameter("param5", pDocumento.Estado.ToString()));
        //         cmd.Parameters.Add(new SqlParameter("param6", pDocumento.Situacion.ToString()));
        //         cmd.CommandTimeout = ConfigurationManager.AppSettings("TiempoEspera").ToString();
        //         _sqlConexion.Open();
        //         var Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        //         if (Rs.HasRows)
        //         {
        //             while (Rs.Read())
        //             {
        //                 oBaja_EN = new EN_Baja();
        //                 oBaja_EN.Id = Rs["id"];
        //                 oBaja_EN.Empresa_id = Rs["empresa_id"];
        //                 oBaja_EN.Tiporesumen = Rs["tiporesumen"];
        //                 oBaja_EN.Correlativo = Rs["correlativo"];
        //                 oBaja_EN.FechaDocumento = Rs["fecgenera_documento"];
        //                 oBaja_EN.FechaComunicacion = Rs["fecgenera_comunicacion"];
        //                 oBaja_EN.Identificacomunicacion = Interaction.IIf(Information.IsDBNull(Rs["identifica_comunicacion"]), "", Rs["identifica_comunicacion"]);
        //                 oBaja_EN.Estado = Rs["estado"];
        //                 oBaja_EN.Situacion = Rs["situacion"];
        //                 oBaja_EN.EnviadoMail = Interaction.IIf(Information.IsDBNull(Rs["enviado_email"]), "NO", Rs["enviado_email"]);
        //                 oBaja_EN.nombreXML = Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
        //                 oBaja_EN.vResumen = Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
        //                 oBaja_EN.vFirma = Interaction.IIf(Information.IsDBNull(Rs["valorfirma"]), "", Rs["valorfirma"]);
        //                 olstBaja.Add(oBaja_EN);
        //             }
        //         }

        //         Rs.Close();
        //         // Guardamos final operacion en el log
        //         objLog.MensajeAuditoriaOk = "listado correctamente";
        //         clsLogAD.GuardarFinLogOperacion(objLog, true);
        //         return olstBaja;
        //     }
        //     catch (Exception ex)
        //     {
        //         // Guardamos error en el log
        //         clsLogAD.GuardarErrorLogOperacion(objLog, ex);
        //         throw ex;
        //     }
        //     finally
        //     {
        //         _sqlConexion.Close();
        //     }
        // }

        // public bool eliminarBaja(EN_Baja pBaja)
        // {
        //     // Llenamos los valores para el LOG
        //     var objLog = new clsLogEN();
        //     objLog.ClaseDatos = "AD_Baja";
        //     objLog.MetodoFuncionClase = "eliminarBaja";
        //     objLog.CadenaAleatoria = pBaja.CadenaAleatoria;
        //     objLog.UsuarioSession = pBaja.UsuarioSession;
        //     var list = new ArrayList();
        //     list.Add("ID: " + pBaja.Id);
        //     // Fin llenado
        //     try
        //     {
        //         _sqlConexion.Open();
        //         _sqlCommand.CommandType = CommandType.StoredProcedure;
        //         _sqlCommand.Connection = _sqlConexion;
        //         _sqlCommand.CommandText = "Usp_eliminar_baja";
        //         _sqlCommand.Parameters.AddWithValue("@idbaja", pBaja.Id);
        //         // Guardamos Log
        //         clsLogAD.GuardarInicioLogOperacion(objLog);
        //         clsLogAD.GuardarLogOperacion(objLog, list);
        //         _sqlCommand.ExecuteNonQuery();
        //         // Guardamos final operacion en el log
        //         objLog.MensajeAuditoriaOk = "baja eliminada correctamente";
        //         clsLogAD.GuardarFinLogOperacion(objLog, true);
        //         return true;
        //     }
        //     catch (Exception ex)
        //     {
        //         // Guardamos error en el log
        //         objLog.MensajeAuditoriaError = "error al eliminar baja";
        //         clsLogAD.GuardarErrorLogOperacion(objLog, ex);
        //         throw ex;
        //     }
        //     finally
        //     {
        //         _sqlConexion.Close();
        //     }
        // } 
    }
}