using Microsoft.AspNetCore.Mvc;
using CEN;
using System;
using AppServicioDoc;
using CAD;
using CLN;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace SAASWeb.Controllers
{
    
    [Produces("application/json")]
    [Route("api/Send")]
    public class EnvioController:Controller
    {
        public EnvioController()
        {
            
        }

        [Route("ExisteRecurso")]
        [HttpGet]
        public ActionResult <Boolean>ExisteRecurso()
        {
           return Ok(true);
        }   

        [Route("Enviodoc")]
        [HttpPost]
        public ClassRptaGenerarCpeDoc EnviodocController([FromBody] EN_RequestEnviodoc request)
        {
            // DESCRIPCION: ENVIO DOCUMENTO ELECTRONICO A SUNAT

            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ; // respuesta Generar comprobante documento
            EN_Documento  oDocumento = new DocumentoSunat(); //documento
            EN_Certificado oCertificado = new EN_Certificado(); //certificado
            GenerarXML objGenerar = new GenerarXML();      //objeto para enviar doc
            var oCompSunat = new EN_ComprobanteSunat(); // comprobante sunat
            EN_Resumenfirma oRF; //resumen firma
            AD_clsLog adlog =  new AD_clsLog();     // clase Log
            NE_Consulta consulta = new NE_Consulta(); // consulta
            EN_Concepto concepto;//entidad concepto
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();  //error
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración
            
            
            Int32 ntraLog = EN_Constante.g_const_0; // Número de transacción
            string estadoCDR="";                    // estado CDR
            string msjCDR="";                       //mensaje CDR
            string vResumen="";                     //valor resumen
            string vFirma="";                       //valor firma
            string produccion = "";                 //producción
            string ruta = "";                       //ruta de documento
            string nombre= "";                      //nombre de archivo XML
            bool envioXml;                          // variable bool a retornar
            string nombreEmpresa="";                //nombre de empresa

            int flagError = EN_Constante.g_const_0; // flag error
            
            try
            {
            
            oDocumento.Id= request.documentoId;
            oDocumento.Idempresa= Convert.ToInt32(request.empresaId);
            oDocumento.Idpuntoventa=Convert.ToInt32(request.Idpuntoventa);
            oDocumento.Idtipodocumento = request.tipodocumentoId;
            oDocumento.Fecha=request.fecha;
            oDocumento.Serie =request.Serie;
            oDocumento.Numero= request.Numero;
            oDocumento.NroDocEmpresa=request.ruc;
           
            oCertificado.UserName= request.certUserName;
            oCertificado.Clave= request.certClave;
            oCertificado.flagOSE = request.flagOse;

            oCompSunat.codigoEtiquetaErrorDoc = EN_ConfigConstantes.Instance.const_codigoEtiquetaErrorDoc;
            produccion = request.Produccion;
            ruta = request.rutaDoc;
            nombre= request.nombreArchivo;

            oRF= objGenerar.GetResumenFirma(oDocumento);
            vResumen= oRF.resumen;
            vFirma= oRF.firma;
           
            ntraLog = adlog.registrar_log_inicio(EN_Constante.g_const_programa, oDocumento.Id, oDocumento.Idtipodocumento, 
                                                oDocumento.Idempresa, oDocumento.Idpuntoventa,  nombreEmpresa, EN_Constante.g_const_1,
                                                EN_Constante.g_const_funcion_generarCpe, EN_Constante.g_const_url, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                confg.getDateNowLima(), confg.getDateHourNowLima(),
                                                EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);
                
          
              //validar parametros obligatorios de entrada
            respuesta = consulta.ValidarCamposServiceDocWA(request);
            if(respuesta.RptaRegistroCpeDoc.FlagVerificacion)
            {
                envioXml= objGenerar.EnviarXmlDocumentoElectronico( oDocumento,  oCertificado,  oCompSunat, produccion, nombre,  vResumen,  vFirma, ref  estadoCDR, ref  msjCDR);

                if(envioXml){
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2003);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= msjCDR;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2003;
                    respuesta.RptaRegistroCpeDoc.estadoComprobante = estadoCDR;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_0, concepto.conceptocorr, concepto.conceptodesc);
                    respuesta.ErrorWebService = ErrorWebSer;

                    flagError= EN_Constante.g_const_0;

                }else{

                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_4000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc  + respuesta.RptaRegistroCpeDoc.DescripcionResp;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_envioxml;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_4000;
                    respuesta.RptaRegistroCpeDoc.estadoComprobante = estadoCDR;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_menos1, concepto.conceptocorr, msjCDR);
                    respuesta.ErrorWebService = ErrorWebSer;

                    flagError= EN_Constante.g_const_1;
                }
                
            }
            else
            {

                   concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_4000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc + " "+ respuesta.RptaRegistroCpeDoc.DescripcionResp;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_4000;
                    respuesta.RptaRegistroCpeDoc.estadoComprobante = EN_Constante.g_const_vacio;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_menos1, concepto.conceptocorr, concepto.conceptodesc);
                    respuesta.ErrorWebService = ErrorWebSer;


                    flagError= EN_Constante.g_const_1;


            }
          
           //registramos log fin
            adlog.registrar_log_fin(ntraLog, flagError, EN_Constante.g_const_2, respuesta.RptaRegistroCpeDoc.DescripcionResp, EN_Constante.g_const_valExito, confg.getDateHourNowLima(),
                                                        JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(request));

            return respuesta;

        
            }
            catch (System.Exception e)
            {
                concepto = new EN_Concepto();
                concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno;
                respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_menos1, concepto.conceptocorr, e.Message.ToString());
                respuesta.ErrorWebService = ErrorWebSer;

                 //registramos log fin
                adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_2, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, e.Message.ToString(), 
                                        confg.getDateHourNowLima(),
                                        JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(request));


                return respuesta;
            }
           

        }


        [Route("EnvioResumenDiario")]
        [HttpPost]
        public ClassRptaGenerarCpeDoc EnvioResDiarioController([FromBody] EN_RequestEnvioResDiario request)
        {
            // DESCRIPCION: ENVIO RESUMEN DIARIO A SUNAT

            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ; // respuesta Generar comprobante documento
            EN_Resumen  oDocumento = new EN_Resumen();                              //documento
            EN_Certificado oCertificado = new EN_Certificado();                     //certificado
            GenerarXML objGenerar = new GenerarXML();                               //objeto para enviar doc
            var oCompSunat = new EN_ComprobanteSunat();                             // comprobante sunat
            EN_Resumenfirma oRF;                                                    //resumen firma
            AD_clsLog adlog =  new AD_clsLog();                                     // clase Log
            NE_Consulta consulta = new NE_Consulta();                               // consulta
            EN_Concepto concepto;                                                   //entidad concepto
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();              //error
            List<EN_DetalleResumen> detalleResumen = new List<EN_DetalleResumen>(); // lista de clase detalle resumen
            NE_Resumen obj = new NE_Resumen();          //clase negocio resumen
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración
              
            Int32 ntraLog = EN_Constante.g_const_0;     // Número de transacción
            string estadoCDR="";                        // estado CDR
            string msjCDR="";                           //mensaje CDR
            string vResumen="";                         //valor resumen
            string vFirma="";                           //valor firma
            string produccion = "";                     //producción
            string ruta = "";                           //ruta de documento
            string nombre= "";                          //nombre de archivo XML
            bool envioXml;                              // variable bool a retornar
            string nombreEmpresa="";                    //nombre de empresa

            int flagError = EN_Constante.g_const_0; // flag error
            
            try
            {
            
            oDocumento.Id= Convert.ToInt32(request.resumenId);
            oDocumento.Empresa_id= Convert.ToInt32(request.empresaId);
            oDocumento.Tiporesumen = request.tiporesumenId;
            oDocumento.FechaGeneraResumen=request.FechaGeneraResumen;
            oDocumento.Correlativo = Convert.ToInt32(request.correlativo);
            oDocumento.Nrodocumento=request.ruc;
            
           
            oCertificado.UserName= request.certUserName;
            oCertificado.Clave= request.certClave;
            oCertificado.flagOSE = request.flagOse;

            oCompSunat.codigoEtiquetaErrorDoc = EN_ConfigConstantes.Instance.const_codigoEtiquetaErrorDoc;
            oCompSunat.codigoEtiquetaErrorRes = EN_ConfigConstantes.Instance.const_codigoEtiquetaErrorRes;
            produccion = request.Produccion;
            ruta= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos+"/"+oDocumento.Nrodocumento+"/";
            nombre= request.nombreArchivo;

            oRF= objGenerar.GetResumenFirmaRD(oDocumento);
            vResumen= oRF.resumen;
            vFirma= oRF.firma;
           
            ntraLog = adlog.registrar_log_inicio(EN_Constante.g_const_programa, oDocumento.Id.ToString(), oDocumento.Tiporesumen.ToString(), 
                                                oDocumento.Empresa_id, Convert.ToInt32(request.Idpuntoventa),  nombreEmpresa, EN_Constante.g_const_1,
                                                EN_Constante.g_const_funcion_generarCpe, EN_Constante.g_const_url, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                 confg.getDateNowLima(), confg.getDateHourNowLima(),
                                                EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);
                
          
              //validar parametros obligatorios de entrada
            respuesta = consulta.ValidarCamposServiceDocRDWA(request);
            if(respuesta.RptaRegistroCpeDoc.FlagVerificacion)
            {
                // Lista el detalle de la baja
                detalleResumen = (List<EN_DetalleResumen>)obj.listarDetalleResumen(oDocumento);
            
                envioXml= objGenerar.EnviarXmlResumenElectronico(oDocumento,
                                                                 detalleResumen,
                                                                 oCertificado,
                                                                 oCompSunat,
                                                                 ruta.ToString(),
                                                                 nombre.ToString(),
                                                                 vResumen.ToString(),
                                                                 vFirma.ToString(),
                                                                 ref estadoCDR,
                                                                 ref msjCDR, produccion.ToString()
                                                                 );
                if(envioXml){

                

                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2003);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= msjCDR;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2003;
                    respuesta.RptaRegistroCpeDoc.estadoComprobante =estadoCDR;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_0, concepto.conceptocorr, concepto.conceptodesc);
                    respuesta.ErrorWebService = ErrorWebSer;

                    flagError = EN_Constante.g_const_0; 

                }else{
                   
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_4000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_envioxml;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_4000;
                    respuesta.RptaRegistroCpeDoc.estadoComprobante = estadoCDR;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_menos1, concepto.conceptocorr, msjCDR +" - "+concepto.conceptodesc);
                    respuesta.ErrorWebService = ErrorWebSer;


                    flagError = EN_Constante.g_const_1; 
                }


                   adlog.registrar_log_fin(ntraLog, flagError, EN_Constante.g_const_2,
                                 respuesta.RptaRegistroCpeDoc.DescripcionResp, 
                                 EN_Constante.g_const_valExito, confg.getDateHourNowLima(),
                                                        JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(request));
                
                
            }
            else
            {

                    
                   //registramos log fin
                adlog.registrar_log_fin(ntraLog, flagError, EN_Constante.g_const_2, respuesta.RptaRegistroCpeDoc.DescripcionResp, EN_Constante.g_const_valExito, confg.getDateHourNowLima(),
                                                        JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(request));



            }
          
        
            return respuesta;

        
            }
            catch (System.Exception e)
            {
                concepto = new EN_Concepto();
                concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno;
                respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                respuesta.RptaRegistroCpeDoc.estadoComprobante = EN_Constante.g_const_vacio;
                ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_menos1, concepto.conceptocorr, e.Message.ToString());
                respuesta.ErrorWebService = ErrorWebSer;

                 //registramos log fin
                adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_2, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, e.Message.ToString(), 
                                            confg.getDateHourNowLima(),
                                        JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(request));


                return respuesta;
            }
           

        }



        [Route("EnvioComunicadoBaja")]
        [HttpPost]
        public ClassRptaGenerarCpeDoc EnvioComBajaController([FromBody] EN_RequestEnvioComBaja request)
        {
            // DESCRIPCION: ENVIO COMUNICADO DE BAJA A SUNAT

            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ;       // respuesta Generar comprobante documento
            BajaSunat  oDocumento = new BajaSunat();                                //documento baja sunat
            EN_Certificado oCertificado = new EN_Certificado();                     //certificado
            GenerarXML objGenerar = new GenerarXML();                               //objeto para enviar doc
            var oCompSunat = new EN_ComprobanteSunat();                             // comprobante sunat
            EN_Resumenfirma oRF;                                                    //resumen firma
            AD_clsLog adlog =  new AD_clsLog();                                     // clase Log
            NE_Consulta consulta = new NE_Consulta();                               // consulta
            EN_Concepto concepto;                                                   //entidad concepto
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();              //error
            List<EN_DetalleBaja> detalleBaja = new List<EN_DetalleBaja>();       // lista de clase detalle baja
            NE_Baja obj = new NE_Baja();                                            //clase negocio baja
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración
              
            Int32 ntraLog = EN_Constante.g_const_0;     // Número de transacción
            string estadoCDR="";                        // estado CDR
            string msjCDR="";                           //mensaje CDR
            string vResumen="";                         //valor resumen
            string vFirma="";                           //valor firma
            string produccion = "";                     //producción
            string ruta = "";                           //ruta de documento
            string nombre= "";                          //nombre de archivo XML
            bool envioXml;                              // variable bool a retornar
            string nombreEmpresa="";                    //nombre de empresa

            int flagError = EN_Constante.g_const_0; // flag error
            
            try
            {
            
            oDocumento.Id= Convert.ToInt32(request.resumenId);
            oDocumento.Empresa_id= Convert.ToInt32(request.empresaId);
            oDocumento.Tiporesumen = request.tiporesumenId;
            oDocumento.FechaComunicacion=request.FechaComunicacion;
            oDocumento.Correlativo = Convert.ToInt32(request.correlativo);
            oDocumento.Nrodocumento=request.ruc;
            
           
            oCertificado.UserName= request.certUserName;
            oCertificado.Clave= request.certClave;
            oCertificado.flagOSE = request.flagOse;

            oCompSunat.codigoEtiquetaErrorDoc = EN_ConfigConstantes.Instance.const_codigoEtiquetaErrorDoc;
            oCompSunat.codigoEtiquetaErrorRes = EN_ConfigConstantes.Instance.const_codigoEtiquetaErrorRes; //

            produccion = request.Produccion;
            ruta= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos+"/"+oDocumento.Nrodocumento+"/";
            nombre= request.nombreArchivo;

            oRF= objGenerar.GetResumenFirmaCB(oDocumento);
            vResumen= oRF.resumen;
            vFirma= oRF.firma;
           
            ntraLog = adlog.registrar_log_inicio(EN_Constante.g_const_programa, oDocumento.Id.ToString(), oDocumento.Tiporesumen.ToString(), 
                                                oDocumento.Empresa_id, Convert.ToInt32(request.Idpuntoventa),  nombreEmpresa, EN_Constante.g_const_1,
                                                EN_Constante.g_const_funcion_generarCpe, EN_Constante.g_const_url, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                confg.getDateNowLima(), confg.getDateHourNowLima(),
                                                EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);
                
          
              //validar parametros obligatorios de entrada
            respuesta = consulta.ValidarCamposServiceDocCBWA(request);
            // respuesta.RptaRegistroCpeDoc.FlagVerificacion=true;
            if(respuesta.RptaRegistroCpeDoc.FlagVerificacion)
            {
                // Lista el detalle de la baja
                detalleBaja = (List<EN_DetalleBaja>)obj.listarDetalleBaja(oDocumento);

                envioXml= objGenerar.EnviarXmlBajasElectronico(oDocumento,
                                                                 detalleBaja,
                                                                 oCertificado,
                                                                 oCompSunat,
                                                                 ruta.ToString(),
                                                                 nombre.ToString(),
                                                                 vResumen.ToString(),
                                                                 vFirma.ToString(),
                                                                 ref estadoCDR,
                                                                 ref msjCDR, produccion.ToString()
                                                                 );
                if(envioXml){

                

                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2003);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= msjCDR;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2003;
                    respuesta.RptaRegistroCpeDoc.estadoComprobante = estadoCDR;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_0, concepto.conceptocorr, concepto.conceptodesc);
                    respuesta.ErrorWebService = ErrorWebSer;
                    
                    flagError = EN_Constante.g_const_0;
                }else{
                   
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_4001);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_envioxml;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_4001;
                    respuesta.RptaRegistroCpeDoc.estadoComprobante =estadoCDR;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_menos1, concepto.conceptocorr,msjCDR);
                    respuesta.ErrorWebService = ErrorWebSer;

                    flagError = EN_Constante.g_const_1;
                }

                adlog.registrar_log_fin(ntraLog, flagError, EN_Constante.g_const_2,
                                 respuesta.RptaRegistroCpeDoc.DescripcionResp, 
                                 EN_Constante.g_const_valExito, confg.getDateHourNowLima(),
                                                        JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(request));
                
            }
            else
            {

                
                  //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_2, 
                                    EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, respuesta.ErrorWebService.DescripcionErr,
                                     confg.getDateHourNowLima(),
                                     JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(request));

    


            }

       

            return respuesta;

        
            }
            catch (System.Exception e)
            {
                concepto = new EN_Concepto();
                concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno;
                respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                respuesta.RptaRegistroCpeDoc.estadoComprobante = EN_Constante.g_const_vacio;
                ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_menos1, concepto.conceptocorr, e.Message.ToString());
                respuesta.ErrorWebService = ErrorWebSer;

                 //registramos log fin
                adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_2, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, e.Message.ToString(), 
                                        confg.getDateHourNowLima(),
                                        JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(request));


                return respuesta;
            }
           

        }
        
    }
}