﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: NE_Documento.cs
 VERSION : 1.0
 OBJETIVO: Clase de capa de negocio documento
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using CEN;
using CAD;

namespace CLN
{


    public class NE_Documento
    {

        public string consultarSituacion(EN_Documento pDocumento)
        {
             // DESCRIPCION: CONSULTAR SITUACION

            var obj = new AD_Documento(); // CLASE DE CONEXION DOCUMENTO

            try
            {
                obj = new AD_Documento();
                return obj.consultarSituacion(pDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public EN_Resumenfirma GetResumenFirma(EN_Documento oDocumento)
        {
            // DESCRIPCION: RESUMEN FIRMA

            var obj = new AD_Documento(); // CLASE DE CONEXION DOCUMENTO
             try
            {
                return obj.GetResumenFirma(oDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public EN_Resumenfirma GetResumenFirmaRD(EN_Resumen oDocumento)
        {
            // DESCRIPCION: RESUMEN FIRMA RESUMEN DIARIO

            var obj = new AD_Documento(); // CLASE DE CONEXION DOCUMENTO

             try
            {
                return obj.GetResumenFirmaRD(oDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

         public EN_Resumenfirma GetResumenFirmaCB(BajaSunat oDocumento)
        {
            // DESCRIPCION: RESUMEN FIRMA RESUMEN DIARIO

            var obj = new AD_Documento(); // CLASE DE CONEXION DOCUMENTO

             try
            {
                return obj.GetResumenFirmaCB(oDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

     
  
       
     
    }

}
