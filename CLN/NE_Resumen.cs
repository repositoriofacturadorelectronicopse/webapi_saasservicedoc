﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: NE_Resumen.cs
 VERSION : 1.0
 OBJETIVO: Clase de negocio resumen
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

using System;
using CAD;
using CEN;


namespace CLN
{
    public class NE_Resumen
    {
  
    public object listarDetalleResumen(EN_Resumen pResumen)
    {
        // DESCRIPCION: LISTAR DETALLE RESUMEN
        
        AD_Resumen obj = new AD_Resumen();  // CLASE CONEXION RESUMEN
        try
        {
            return obj.listarDetalleResumen(pResumen);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
  
    public void GuardarResumenAceptada(EN_Resumen oResumen, string nombrexml, string xmlbase64, string cdrxml, string valorResumen, string firma, string mensaje, string ticket)
    {
        // DESCRIPCION: GUARDAR RESUMEN ACEPTADA
        
        AD_Resumen obj = new AD_Resumen();  // CLASE CONEXION RESUMEN

        try
        {
            obj.GuardarResumenAceptada(oResumen, nombrexml, xmlbase64, cdrxml, valorResumen, firma, mensaje, ticket);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void GuardarResumenRechazadaError(EN_Resumen oResumen, string nombrexml, string xmlbase64, string cdrxml, string valorResumen, string firma, string errortext, string estado, string ticket)
    {
        // DESCRIPCION: GUARDAR RESUMEN RECHAZADA ERROR
        
        AD_Resumen obj = new AD_Resumen();  // CLASE CONEXION RESUMEN
        try
        {
            obj.GuardarResumenRechazadaError(oResumen, nombrexml, xmlbase64, cdrxml, valorResumen, firma, errortext, estado, ticket);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    }
}