/****************************************************************************************************************************************************************************************
 PROGRAMA: NE_Consulta.cs
 VERSION : 1.0
 OBJETIVO: Clase de consultas
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using CAD;
using CEN;
namespace CLN
{
    public class NE_Consulta
    {
        
    

    public EN_ErrorWebService LlenarErrorWebSer(short TipoErr, int CodigoErr, string DescripcionErr)
    {
        //DESCRIPCIÓN: Llenar clase de error web service
        EN_ErrorWebService data = new EN_ErrorWebService(); //Clase error weservice
        try
        {
            data.TipoError = TipoErr;
            data.CodigoError = CodigoErr;
            data.DescripcionErr = DescripcionErr;
            return data;
        }
        catch(Exception ex)
        {
            throw ex;
        }
        
    }
    public EN_Concepto ObtenerDescConcepto(int prefijo_cpe_doc, int const_error)
    {
         //DESCRIPCIÓN: Obtener descripcion de conceptos.
        AD_Concepto cad_consentimiento = new AD_Concepto();//Concepto
        EN_Concepto gbcon = new EN_Concepto();//entidad concepto
        
        gbcon = cad_consentimiento.Obtener_desc_gbcon(prefijo_cpe_doc,const_error);
        return gbcon;
    }
    public ClassRptaGenerarCpeDoc ValidarCamposServiceDocWA(EN_RequestEnviodoc documento)
    {
        //DESCRIPCIÓN: Validar campos generar comprobante electronico documentos.

        ClassRptaGenerarCpeDoc RespuestaCpeDoc = new ClassRptaGenerarCpeDoc();//Respuesta
        // AD_Concepto cad_consentimiento = new AD_Concepto();//Concepto
        EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();//Error
        EN_Concepto gbcon = new EN_Concepto();//entidad concepto

        try
        {


            if (documento.flagOse == null || documento.flagOse.Trim().Length == EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_flagOse;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.certUserName == null || documento.certUserName.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_flagOse;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.certClave == null || documento.certClave.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_certClave;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

             else if (documento.nombreArchivo == null || documento.nombreArchivo.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_nombreArchivo;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
             else if (documento.empresaId == null || documento.empresaId.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_empresaId;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

            else  if (documento.documentoId == null || documento.documentoId.Trim().Length == EN_Constante.g_const_0 || documento.documentoId.Trim().Length != EN_Constante.g_const_length_id_cpe_doc)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_documentoId;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

            else  if (documento.tipodocumentoId == null || documento.tipodocumentoId.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_tipodocumentoId;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
              else if (documento.Produccion == null || documento.Produccion.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_Produccion;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.fecha == null || documento.fecha.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_fecha;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.Numero == null || documento.Numero.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_Numero;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
             else if (documento.Serie == null || documento.Serie.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_Serie;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
          
            gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2000);
            RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = gbcon.conceptodesc;
            RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = true;
            RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
            ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
            RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
            return RespuestaCpeDoc;
        }
        catch(Exception ex)
        {
            throw ex;
        }
        

    }

    public ClassRptaGenerarCpeDoc ValidarCamposServiceDocRDWA(EN_RequestEnvioResDiario documento)
    {
        //DESCRIPCIÓN: Validar campos generar comprobante electronico documentos.

        int conteoNoAcptados = 0;                           // CONTEO DE DOCUMENTOS NO ACEPTADOS
        string docsPendientes ="";                          // DOCUMENTOS PENDIENTES DE ENVIO

        ClassRptaGenerarCpeDoc RespuestaCpeDoc = new ClassRptaGenerarCpeDoc();//Respuesta
        EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();//Error
        EN_Concepto gbcon = new EN_Concepto();//entidad concepto

        try
        {


            if (documento.flagOse == null || documento.flagOse.Trim().Length == EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_flagOse;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.certUserName == null || documento.certUserName.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_flagOse;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.certClave == null || documento.certClave.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_certClave;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
       

             else if (documento.nombreArchivo == null || documento.nombreArchivo.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_nombreArchivo;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
             else if (documento.empresaId == null || documento.empresaId.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_empresaId;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

            else  if (documento.resumenId == null || documento.resumenId.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_documentoId;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

            else  if (documento.tiporesumenId== null || documento.tiporesumenId.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_tipodocumentoId;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
              else if (documento.Produccion == null || documento.Produccion.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_Produccion;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.FechaGeneraResumen == null || documento.FechaGeneraResumen.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_fecha;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.correlativo == null || documento.correlativo.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_Numero;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            EN_RequestEnvioComBaja oDocumentoCB = new EN_RequestEnvioComBaja();
            oDocumentoCB.resumenId=documento.resumenId;
            oDocumentoCB.empresaId= documento.empresaId;
            oDocumentoCB.Idpuntoventa= documento.Idpuntoventa;
             VerificarAceptadosComBaja(oDocumentoCB, ref conteoNoAcptados,ref docsPendientes, EN_Constante.g_const_1);

            if(conteoNoAcptados > 0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1007);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = conteoNoAcptados +" "+ EN_Constante.g_const_err_cpependientesenvio_cb + " "+docsPendientes.TrimEnd(',');
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1007;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
               
            }
            
            gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2000);
            RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = gbcon.conceptodesc;
            RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = true;
            RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
            ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
            RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
            return RespuestaCpeDoc;
        }
        catch(Exception ex)
        {
            throw ex;
        }
        

    }

    public void VerificarAceptadosComBaja(EN_RequestEnvioComBaja oDocumento, ref int conteoNoAcptados, ref string docsPendientes , int flag)
    {
        // DESCRIPCION: CONSULTA SITUACION DE COMPROBANTE PARA PROCEDER A COMUNICADO DE BAJA

        NE_Facturacion facturacion = new NE_Facturacion();                          // CLASE NEGOCIO FACTURACION DE
        int IdBaja =0  ;                                                            // ID DE DOCUMENTOS
        int empresa_id = 0;                                                         // ID DE EMPRESA
        int puntoventa_id = 0 ;                                                     // ID PUNTO DE VENTA
        
        List<EN_SituacionBaja> ListaDetalleBaja = new List<EN_SituacionBaja>();     // Lista Detalle Baja

        try
        {
            IdBaja = Convert.ToInt32(oDocumento.resumenId);
            empresa_id = Convert.ToInt32(oDocumento.empresaId);
            puntoventa_id = Convert.ToInt32(oDocumento.Idpuntoventa);

             ListaDetalleBaja = facturacion.consultarSituacionCpeCB( IdBaja ,empresa_id, puntoventa_id, flag);


            // // oDocumento.ListaDetalleBaja[0]["Tipodocumentoid"]=12;
            if(ListaDetalleBaja.Count > 0)
            {
                foreach (var itemDD in ListaDetalleBaja)
                {

                  
                    if(itemDD.situacion != EN_Constante.g_const_situacion_aceptado)
                    {
                        conteoNoAcptados= conteoNoAcptados+ 1;
                        docsPendientes= docsPendientes + itemDD.series.Trim() +",";
                    }

                }
            }

            
        }
        catch (System.Exception ex)
        {
            
            throw ex; 
        }
    }

     public ClassRptaGenerarCpeDoc ValidarCamposServiceDocCBWA(EN_RequestEnvioComBaja documento)
    {
        //DESCRIPCIÓN: Validar campos generar comprobante electronico documentos.

         int conteoNoAcptados = 0;                           // CONTEO DE DOCUMENTOS NO ACEPTADOS
        string docsPendientes ="";                          // DOCUMENTOS PENDIENTES DE ENVIO

        ClassRptaGenerarCpeDoc RespuestaCpeDoc = new ClassRptaGenerarCpeDoc();//Respuesta
        EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();//Error
        EN_Concepto gbcon = new EN_Concepto();//entidad concepto

        try
        {


            if (documento.flagOse == null || documento.flagOse.Trim().Length == EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_flagOse;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.certUserName == null || documento.certUserName.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_flagOse;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.certClave == null || documento.certClave.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_certClave;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
         

             else if (documento.nombreArchivo == null || documento.nombreArchivo.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_nombreArchivo;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
             else if (documento.empresaId == null || documento.empresaId.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_empresaId;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

             else if (documento.Idpuntoventa == null || documento.Idpuntoventa.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_puntoventaId;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

            else  if (documento.resumenId == null || documento.resumenId.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_documentoId;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

            else  if (documento.tiporesumenId== null || documento.tiporesumenId.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_tipodocumentoId;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
              else if (documento.Produccion == null || documento.Produccion.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_Produccion;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.FechaComunicacion == null || documento.FechaComunicacion.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_fecha;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.correlativo == null || documento.correlativo.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_Numero;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }


            VerificarAceptadosComBaja(documento, ref conteoNoAcptados,ref docsPendientes, EN_Constante.g_const_0);

            if(conteoNoAcptados > 0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1007);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = conteoNoAcptados +" "+ EN_Constante.g_const_err_cpependientesenvio_cb + " "+docsPendientes.TrimEnd(',');
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1007;
                ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
               
            }

            
            gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2000);
            RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = gbcon.conceptodesc;
            RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = true;
            RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
            ErrorWebSer = LlenarErrorWebSer(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
            RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
            return RespuestaCpeDoc;
        }
        catch(Exception ex)
        {
            throw ex;
        }
        

    }

    }
}