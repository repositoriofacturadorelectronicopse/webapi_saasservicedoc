﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: NE_Facturacion.cs
 VERSION : 1.0
 OBJETIVO: Clase de negocio facturación
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

using System;
using System.Collections.Generic;
using CAD;
using CEN;

namespace CLN
{
  

    public partial class NE_Facturacion
    {
        private string pClase = "NE_Facturacion";

        public string buscarParametroAppSettings(int codconcepto,int codcorrelativo)
        {
            // DESCRIPCION: BUSCAR PARAMETRO DE CONFIGURACION

            var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION

            try
            {
                return obj.buscarParametroAppSettings(codconcepto,codcorrelativo);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string buscarConstantesTablaSunat(string codtabla,string codalterno)
        {
            // DESCRIPCION: BUSCAR CONSTANTES TABLA SUNAT

             var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION

             try
            {
                return obj.buscarConstantesTablaSunat(codtabla,codalterno);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }

        }

        public string ObtenerDescripcionPorCodElemento(string codtabla, string codigo)
        {
            // DESCRIPCION: BUSCAR DESCRIPCION POR CODIGO ELEMENTO

            var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION
            try
            {
                return obj.ObtenerDescripcionPorCodElemento(codtabla, codigo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

     

        public string ObtenerCodigoAlternoPorCodigoSistema(string codtabla, string codigo)
        {
            // DESCRIPCION: BUSCAR CODIGO ALTERNO POR CODIGO SISTEMA

            var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION

            try
            {
                return obj.ObtenerCodigoAlternoPorCodigoSistema(codtabla, codigo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    public void GuardarDocumentoAceptado(EN_Documento oDocumento, string nombrexml, string xmlenviado, string cdrxml, string valorResumen, string firma, string mensaje, string ticket)
    {
        // DESCRIPCION: GUARDAR DOCUMENTO ACEPTADO

        var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION
        try
        {
            obj.GuardarDocumentoAceptado(oDocumento, nombrexml, xmlenviado, cdrxml, valorResumen, firma, mensaje, ticket);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

     public void GuardarDocumentoObservado(EN_Documento oDocumento, string nombrexml, string xmlenviado, string cdrxml, string valorResumen, string firma, string mensaje, string ticket, string situacion)
    {
        // DESCRIPCION: GUARDAR DOCUMENTO ACEPTADO

        var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION
        try
        {
            obj.GuardarDocumentoObservado(oDocumento, nombrexml, xmlenviado, cdrxml, valorResumen, firma, mensaje, ticket, situacion);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    public void GuardarDocumentoRechazadoError(EN_Documento oDocumento, string nombrexml, string xmlenviado, string cdrxml, string valorResumen, string firma, string errortext, string estado, string ticket)
    {
        // DESCRIPCION: GUARDAR DOCUMENTO RECHAZADO ERROR

        var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION

        try
        {
            obj.GuardarDocumentoRechazadoError(oDocumento, nombrexml, xmlenviado, cdrxml, valorResumen, firma, errortext, estado, ticket);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


        public bool ActualizarSituacion(EN_Documento oDocumento, string situacion)
        {
                // DESCRIPCION: ACTUALIZAR SITUACION

                var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION
                try
                {
                    return obj.ActualizarSituacion(oDocumento, situacion);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
        }

    
        public List<EN_SituacionBaja> consultarSituacionCpeCB(int id, int empresa_id, int puntoventa_id, int flag)
        {
            // DESCRIPCION: CONSULTA SITUACION DE COMPROBANTE PARA PROCEDER A COMUNICADO DE BAJA

             var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION
            try
            {
                return obj.consultarSituacionCpeCB( id,  empresa_id,  puntoventa_id, flag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }





  

    }
}
